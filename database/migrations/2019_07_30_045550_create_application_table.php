<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('topic');
            $table->text('theme');


            $table->string('abstract_file');
            $table->string('full_paper')->nullable();
            $table->string('status');
            $table->string('payment_method')->nullable();
            $table->string('payment_voucher_file')->nullable();
            $table->string('paid_status')->default('unpaid');
            $table->string('payment_details')->nullable();

            $table->integer('event_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
