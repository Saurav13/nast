<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ParticipantController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $users = User::where('email_verified_at','!=','')->orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.participants.index')->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'title' => 'required|max:191',
            'user_date' => 'required|date',
            'description' => 'required',
            'location' => 'required',
            'image' => 'required|image'
        ]);

        $_users = new User;
        $_users->title = $request->title;
        $_users->description = $request->description;
        $_users->location = $request->location;
        $_users->user_date = $request->user_date;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('users_images/');
            $photo->move($location,$filename);
            $_users->image = $filename;
        }

        $_users->save();

        $request->session()->flash('success', 'User added.');        
        
        return redirect()->route('participants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $_users = User::findOrFail($id);

        return view('admin.participants.show',compact('_users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $_users = User::findOrFail($id);

        return view('admin.participants.edit',compact('_users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'title' => 'required|max:191',
            'description' => 'required',
            'location' => 'required',
            'user_date' => 'required|date',
            'image' => 'nullable|image'
        ]);

        $_users = User::findOrFail($id);
       
        $_users->title = $request->title;

        $_users->description = $request->description;
        $_users->location = $request->location;

        $_users->user_date = $request->user_date;
        if($request->hasFile('image')){

            if($_users->image)
                unlink(public_path('users_images/'.$_users->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('users_images/');
            $photo->move($location,$filename);
            $_users->image = $filename;
        }

        $_users->save();

        $request->session()->flash('success', 'User updated.');        
        
        return redirect()->route('participants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $_users = User::findOrFail($id);

        if($_users->image)
            unlink(public_path('users_images/'.$_users->image));

        $_users->delete();

        $request->session()->flash('success', 'User deleted.');        
        
        return redirect()->route('participants.index');
    }

    public function print($id)
    {
        $user = User::findOrFail($id);
        return view('admin.participants.print',compact('user'));
        
    }
}
