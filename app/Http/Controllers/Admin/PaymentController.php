<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use Session;

class PaymentController extends Controller
{
    public function index()
    {
        $payments=Payment::where('status','!=','Temp')->orderBy('status','DESC')->paginate(20);
        return view('admin.payments.index', compact('payments'));
    }
    public function getFile($id)
    {
        $filename=Payment::findOrFail($id)->file;

        try{
            $path = \Storage::get('public/vouchers/'.$filename);
            $mimetype = \Storage::mimeType('public/vouchers/'.$filename);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }
    public function verify($id)
    {
        // dd($id);
        Payment::where('id',$id)->update(['status'=>'paid']);
        Session::flash('success','Payment verified successfully.');
        return redirect()->to('/admin/payments');   
    }
    public function reject($id)
    {
        // dd($id);
        Payment::where('id',$id)->update(['status'=>'rejected']);
        Session::flash('success','Payment rejected successfully.');
        return redirect()->to('/admin/payments');  
    }

}
