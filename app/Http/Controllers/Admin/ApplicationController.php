<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Application;
use App\Mail\ApplicationStatusMail;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Session;
use Redirect;
use Mail;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    public function index(Request $request)
    {
        $event_id=Event::latest()->first()->id;

        $applications = [];

        $topic = $request->topic == 'all' ? null : $request->topic;
        $theme = $request->theme == 'all' ? null : $request->theme;

        $query = Application::where('event_id',$event_id)->where('status','submitted')->orderBy('updated_at','desc');

        if($topic){
            $topics = [
                'Health and Medicine',
                'Water and Energy',
                'Food and Agriculture',
                'Transport and Tourism',
                'Information and Communication Technology',
                'Forest and Environment',
                'Disaster Risk Reduction',
                'Indigenous Knowledge and Technology',
                'Fundamental Sciences',
                'Applied Sciences',
                'Emerging Technology',
                'Clean and Green Sciences',
                'Economics and Management',
                'Policy',
                'Philosophy'
            ];
            if($topic == 'Others')
                $query->whereNotIn('topic',$topics);
            else {
                $query->where('topic',$topic);
            }
        }

        if($theme){

            $_applications = $query->get();

            foreach($_applications as $n){
                $themes = json_decode($n->theme);

                if(in_array($theme,$themes)){
                    $applications []= $n;
                }
            }
            $applications = $this->paginate($applications, $perPage = 10, $page = null, $options = ['path' => route('applications.index')]);

        }else{
            $applications = $query->paginate(10);
        }
       
        return view('admin.applications.index')->with('applications',$applications);
    }


    public function all(Request $request)
    {
        $applications = [];

        $topic = $request->topic == 'all' ? null : $request->topic;
        $theme = $request->theme == 'all' ? null : $request->theme;

        $query = Application::orderBy('updated_at','desc');

        if($topic){
            $topics = [
                'Health and Medicine',
                'Water and Energy',
                'Food and Agriculture',
                'Transport and Tourism',
                'Information and Communication Technology',
                'Forest and Environment',
                'Disaster Risk Reduction',
                'Indigenous Knowledge and Technology',
                'Fundamental Sciences',
                'Applied Sciences',
                'Emerging Technology',
                'Clean and Green Sciences',
                'Economics and Management',
                'Policy',
                'Philosophy'
            ];
            if($topic == 'Others')
                $query->whereNotIn('topic',$topics);
            else {
                $query->where('topic',$topic);
            }
        }

        if($theme){

            $_applications = $query->get();

            foreach($_applications as $n){
                $themes = json_decode($n->theme);

                if(in_array($theme,$themes)){
                    $applications []= $n;
                }
            }
            $applications = $this->paginate($applications, $perPage = 10, $page = null, $options = ['path' => route('applications.all')]);

        }else{
            $applications = $query->paginate(10);
        }

        return view('admin.applications.all')->with('applications',$applications);
    }

    public function store(Request $request)
    {
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $_applications = Application::findOrFail($id);

        return view('admin.applications.show',compact('_applications'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $_applications = Application::findOrFail($id);

        return view('admin.applications.edit',compact('_applications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'title' => 'required|max:191',
            'description' => 'required',
            'application_date' => 'required|date',
            'image' => 'nullable|image'
        ]);

        $_applications = Application::findOrFail($id);
       
        $_applications->title = $request->title;

        $_applications->description = $request->description;

        $_applications->application_date = $request->application_date;
        
        if($request->hasFile('image')){

            if($_applications->image)
                unlink(public_path('application_images/'.$_applications->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('application_images/');
            $photo->move($location,$filename);
            $_applications->image = $filename;
        }

        $_applications->save();

        $request->session()->flash('success', 'Application updated.');        
        
        return redirect()->route('applications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $_applications = Application::findOrFail($id);

        if($_applications->image)
            unlink(public_path('application_images/'.$_applications->image));

        $_applications->delete();

        $request->session()->flash('success', 'Application deleted.');        
        
        return redirect()->route('applications.index');
    }

    public function accept($id)
    {
        $_application = Application::findOrFail($id);
        $_application->status="accepted";
        $_application->save();
        Session::flash('success', 'Successfully marked as accpeted.');   

        Mail::send(new ApplicationStatusMail($_application));
        
        return redirect()->route('applications.index');

    }

    public function reject($id)
    {
        $_application = Application::findOrFail($id);
        $_application->status="rejected";
        $_application->save();
        Session::flash('success', 'Successfully marked as rejected.');   

        Mail::send(new ApplicationStatusMail($_application));
        
        return redirect()->route('applications.index');

    }

    public function getFile($id,$filename)
    {
        $_application = Application::findOrFail($id);

        if($_application->status == 'submitted'){
            $_application->status="under review";
            $_application->save();

            Mail::send(new ApplicationStatusMail($_application));
        }

        $filename=$_application->abstract_file;

        try{
            $path = \Storage::get('public/abstract_files/'.$filename);
            $mimetype = \Storage::mimeType('public/abstract_files/'.$filename);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }

    private function paginate($items, $perPage = 15, $page = null, $options = [])
	{
		$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
		$items = $items instanceof Collection ? $items : Collection::make($items);
		return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	}
}
