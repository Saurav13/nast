<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Session;
use Redirect;

class EventController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $events = Event::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.events.index')->with('events',$events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'title' => 'required|max:191',
            'event_date' => 'required|date',
            'description' => 'required',
            'location' => 'required',
            'image' => 'required|image'
        ]);

        $_events = new Event;
        $_events->title = $request->title;
        $_events->description = $request->description;
        $_events->location = $request->location;
        $_events->event_date = $request->event_date;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('events_images/');
            $photo->move($location,$filename);
            $_events->image = $filename;
        }

        $_events->save();

        $request->session()->flash('success', 'Event added.');        
        
        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $_events = Event::findOrFail($id);

        return view('admin.events.show',compact('_events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $_events = Event::findOrFail($id);

        return view('admin.events.edit',compact('_events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'title' => 'required|max:191',
            'description' => 'required',
            'location' => 'required',
            'event_date' => 'required|date',
            'image' => 'nullable|image'
        ]);

        $_events = Event::findOrFail($id);
       
        $_events->title = $request->title;

        $_events->description = $request->description;
        $_events->location = $request->location;

        $_events->event_date = $request->event_date;
        if($request->hasFile('image')){

            if($_events->image)
                unlink(public_path('events_images/'.$_events->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('events_images/');
            $photo->move($location,$filename);
            $_events->image = $filename;
        }

        $_events->save();

        $request->session()->flash('success', 'Event updated.');        
        
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $_events = Event::findOrFail($id);

        if($_events->image)
            unlink(public_path('events_images/'.$_events->image));

        $_events->delete();

        $request->session()->flash('success', 'Event deleted.');        
        
        return redirect()->route('events.index');
    }
}
