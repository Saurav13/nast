<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Payment;
use App\Event;
use Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $application=Auth::user()->application;
        // dd($events);
        return view('frontend.profile',compact('application'));
    }
    public function getFile($id,$filename)
    {
        $filename=Auth::user()->applications()->findOrFail($id)->abstract_file;
    

        try{
            $path = \Storage::get('public/abstract_files/'.$filename);
            $mimetype = \Storage::mimeType('public/abstract_files/'.$filename);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {

            abort(404);
        }
    }
    public function downloadInvoice()
    {
        $user_id=Auth::user()->id;
        $event_id=Event::latest()->first()->id;
        if(Payment::where('user_id',$user_id)->where('event_id',$event_id)->where('status','!=','Temp')->count()>0)
        {
            $payment=Payment::where('user_id',$user_id)->where('event_id',$event_id)->where('status','!=','Temp')->first();
            if($payment->status=="paid")
                return view('frontend.invoice',compact('payment'));
            else    
                abort(404);
        }
        abort(404);
        // $payment=Payment::where('');
    }
    public function uploadVoucher(Request $request)
    {
        $request->validate([
            'voucher'=>'required',
            'user_type'=>'required'
           
        ]);
        $event_id=Event::latest()->first()->id;
        
        if(Payment::where('event_id',$event_id)->where('user_id',Auth::user()->id)->where('status','!=','Temp')->count()>0)
        {
             $payment=Payment::where('event_id',$event_id)->where('user_id',Auth::user()->id)->where('status','!=','Temp')->first();
        }
        else{
            $payment= new Payment;

        }
        $payment->event_id=$event_id;
        $payment->user_id=Auth::user()->id;
        $payment->type="Bank Deposit";
        $payment->user_type=$request->user_type;

        
        
        if($request->hasFile('voucher')){
            $file = $request->file('voucher');
               

            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time().'.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/vouchers/', $filename);
            $payment->file = $filename;
            $payment->status="pending";
            $payment->save();
            Session::flash('success','Voucher Uploaded Successfully');
        }
        return redirect()->to('/profile');
        
    }

    public function esewaInit(Request $request)
    {
    
        $event_id=Event::latest()->first()->id;

        $payment= new Payment;
        $payment->status="Temp";
        $payment->type="Esewa";
        $payment->user_type="Nepalese";
        $payment->event_id=$event_id;
        $payment->user_id=Auth::user()->id;
        $payment->save();

        $tAmt=2000;
        $amt=2000;

        return response()->json(['tAmt'=>$tAmt,'amt'=>$amt, 'p_id'=>$payment->id]);

        


    }
    public function success(Request $request)
    {

        if($request->amt == 2000)
        {
            $url = "https://uat.esewa.com.np/epay/transrec";
            $data =[
                'amt'=> 2000,
                'rid'=> $request->refId,
                'pid'=>$request->oid,
                'scd'=> 'epay_payment'
            ];

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);

            $message = simplexml_load_string($response);
            $status = trim(preg_replace('/\s+/', ' ', $message->response_code));
            // dd($response);
            if($status == "Success"){
                $payment= Payment::findOrFail($request->oid);
                $payment->type="Esewa";
                $payment->user_id=Auth::user()->id;
                $payment->user_type="Nepalese";
                $payment->event_id=Event::latest()->first()->id;
                $payment->t_id=$request->refId;
                $payment->status="paid";
                $payment->save();
                Payment::where('event_id',Event::latest()->first()->id)
                        ->where('user_id',Auth::user()->id)
                        ->where('status','!=','paid')->delete();

                Session::flash('success','Payment Successful');
                return redirect()->to('/profile');
            }
            else{
                Session::flash('error','We could not verify your payment. Please contact us if you have any issues');
                return redirect()->to('/profile');
            }
        }       
        else{
            Session::flash('error','We could not verify your payment. Please contact us if you have any issues');
            return redirect()->to('/profile');
        }
    }

    public function failure()
    {
        Session::flash('error','Something went wrong we were unable to process your transaction at the moment');
        return redirect()->to('/profile');
    }
}
