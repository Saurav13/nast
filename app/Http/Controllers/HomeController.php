<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\Application;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $event=Event::latest()->first();
        return view('frontend.home',compact('event'));
    }

    public function event()
    {
        $event=Event::latest()->first();
        return view('frontend.event',compact('event'));
    }

    public function guidelines()
    {
        return view('frontend.guidelines');
    }
    public function abstractGuidelines()
    {
        return view('frontend.abstractguidelines');
    }
    public function posterGuidelines()
    {
        return view('frontend.posterguidelines');
    }

    public function immigration()
    {
        return view('frontend.immigration');
    }

    public function committees()
    {
        return view('frontend.committees');
    }

    public function about()
    {
        return view('frontend.about');
    }
    public function contact()
    {
        return view('frontend.contact');
    }


    public function apply()
    {
        $application=User::findOrFail(Auth::user()->id)->application;
        // dd($application);
        return view('frontend.apply',compact('application'));
    }

    public function reApply()
    {
        $application=User::findOrFail(Auth::user()->id)->application;
        $application->status="reapplying";
        $application->save();
        
        return redirect()->to('/apply');
    }



    public function abstractSubmission(Request $request)
    {
        if(Auth::user()->applications()->where('event_id',Event::latest()->first()->id)->exists()){
            $request->session()->flash('error', 'You have already applied to this event.');

            return redirect('apply');
        }

        $request->validate([
            'title'=>'required|max:150',
            'topic'=>'required',
            'other_value' => 'required_if:topic,Others',
            'theme'=>'required|array|min:1',
            'abstract_file'=>'required|mimes:doc,docx'
        ]);
            
        $application = new Application;
        $application->title=$request->title;
        if($application->topic=="Others")
            $application->topic=$request->other_value;
        else
            $application->topic=$request->topic;
        $application->theme=json_encode($request->theme);
        $application->status='submitted';
        
        if($request->hasFile('abstract_file')){
            $file = $request->file('abstract_file');
               

            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time().'.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/abstract_files/', $filename);
            $application->abstract_file = $filename;
          
        }
        

        $application->event_id=Event::latest()->first()->id;
        $application->user_id=Auth::user()->id;

        $application->save();

        return redirect()->to('/apply');


    }

   


}
