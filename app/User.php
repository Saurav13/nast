<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\UserResetPasswordNotification;
use App\Notifications\UserVerificationNotification;
use App\Payment;
use App\Event;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','gender','age','phone',
        'email', 'password',
        'first_name',
        'middle_name',
        'last_name',
        'designation',
        'dob',
        'address1',
        'address2',
        'city',
        'country',
        'affiliation',
        'af_address',
        'participant_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getNameAttribute(){
        return $this->title. ' '. $this->first_name . ' ' .$this->middle_name. ' '. $this->last_name;
    }

    public function getAddressAttribute()
    {
        return $this->address1.', '.$this->address2.', '.$this->city.', '.$this->country;
    }
    public function getEducationAttribute()
    {
        return $this->affiliation;
    }
    public function getApplicationAttribute()
    {
        $event_id=Event::latest()->first()->id;
        $applications= Application::where('user_id',$this->id)->where('event_id',$event_id)->get();

        if($applications->count()>0)
            return $applications->first();
        else   
            return NULL;
    }

    public function applications()
    {
        return $this->hasMany('App\Application');
    }
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new UserVerificationNotification());
    }
    
    public function getPaidStatusAttribute()
    {
        $event_id=Event::latest()->first()->id;
        if(Payment::where('event_id',$event_id)->where('user_id',$this->id)->where('status','!=','Temp')->count()>0)
        {

            return Payment::where('event_id',$event_id)->where('user_id',$this->id)->where('status','!=','Temp')->first()->status;
        }
        else return "unpaid";
    }
}
