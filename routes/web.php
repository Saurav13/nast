<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'verified'],function(){
    Route::get('/profile', 'Frontend\UserController@index')->name('home');
    Route::get('/download-invoice', 'Frontend\UserController@downloadInvoice');

    Route::post('/upload-voucher', 'Frontend\UserController@uploadVoucher');
  
    Route::get('/getfile/{id}/{filename}','Frontend\UserController@getFIle');
    Route::post('/abstract-submission','HomeController@abstractSubmission')->name('abstract-submission');
    Route::get('/apply','HomeController@apply');
    
    Route::get('/esewa-init','Frontend\UserController@esewaInit');
    Route::get('/payment/esewa/esewa_payment/success', 'Frontend\UserController@success')->name('esewa.success');
    Route::get('/payment/esewa/esewa_payment/failure', 'Frontend\UserController@failure')->name('esewa.failure');

});

Route::get('/guidelines','HomeController@guidelines');
Route::get('/abstract-guidelines','HomeController@abstractGuidelines');
Route::get('/poster-guidelines','HomeController@posterGuidelines');
Route::get('/brochure',function(){
    return view('frontend.brochure');
});

Route::get('/committees','HomeController@committees');
Route::get('/immigration','HomeController@immigration');

Route::get('/conference','HomeController@event');
Route::get('/about','HomeController@about');
Route::get('/contact','HomeController@contact');


Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::resource('events','Admin\EventController');
    Route::get('/applications/viewall','Admin\ApplicationController@all')->name('applications.all');
    Route::get('/applications/getfile/{id}/{filename}','Admin\ApplicationController@getfile');

    Route::get('/applications/{id}/accept','Admin\ApplicationController@accept');
    Route::get('/applications/{id}/reject','Admin\ApplicationController@reject');
    Route::resource('applications', 'Admin\ApplicationController');
    Route::get('/participant/{id}/print','Admin\ParticipantController@print');

    Route::resource('participants', 'Admin\ParticipantController');
    Route::get('/payments/{id}/getfile','Admin\PaymentController@getFile');
    Route::get('/payments/{id}/verify','Admin\PaymentController@verify');
    Route::get('/payments/{id}/reject','Admin\PaymentController@reject');

    Route::resource('payments', 'Admin\PaymentController');


    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);



    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');

    Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');
    Route::post('test','Admin\AdminController@test')->name('test');

    Route::get('/','Admin\AdminController@index');
});

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    $ext = explode(".",$img);
    $ext = end($ext);
    $source = str_replace('*','/',$source);
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('optimize');
