@extends('layouts.app')

@section('body')
<div class="container g-py-100">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <br>
            <div class="card">
                <div class="card-header">Welcome {{Auth::user()->name}} !!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You can now apply to our conference.<br><br>
                        <a class="btn btn-primary" href="/apply">Apply</a> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
