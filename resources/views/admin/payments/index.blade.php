@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>

   <div class="content-body">
   

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Payments</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Gender</th>
                                   <th>Address</th>
                                   <th>Participant Type</th>
                                   <th>Payment Method</th>
                                   <th> Nationality</th>
                                   <th>Status</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($payments as $payment)
                               <tr>
                                   <td>{{ $loop->iteration + (($payments->currentPage()-1) * $payments->perPage()) }}</td>
                                   <td>{{ $payment->user->name }}</td>
                                   <td>{{ $payment->user->gender }}</td>
                                   <td>{{ $payment->user->address }}</td>
                                   {{-- <td>{{ $payment->user->participant_type }}</td> --}}
                                   <td>{{ $payment->type }}</td>
                                   <td>{{ $payment->user_type }}</td>
                                   <td>{{ $payment->status }}</td>
                                   <td>
                                    @if($payment->type=="Bank Deposit")
                                        <a class="btn btn-outline-primary" title="Download File" download href="/admin/payments/{{$payment->id}}/getfile"><i class="icon-download"></i></a>
                                    @endif
                                    @if($payment->status=="pending" || $payment->status=="reject" )

                                        <a class="btn btn-outline-success" title="Verify"  href="/admin/payments/{{$payment->id}}/verify"><i class="icon-check"></i></a>
                                    @endif
                                    @if($payment->status=="pending")

                                   <a class="btn btn-outline-danger" title="Reject"  href="/admin/payments/{{$payment->id}}/reject"><i class="icon-cross"></i></a>
                                    @endif
                                      
                                     
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $payments->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });
   </script>
  
@endsection