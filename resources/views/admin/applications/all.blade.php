@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>

   <div class="content-body">
       <div class="container">
        <a href="/admin/applications/" class="btn btn-primary" style="float:right;margin-bottom:1rem;">View Submitted Abstracts</a>
       </div>
       <div class="card">
           
           <div class="card-header">
               <h4 class="card-title">Applications</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                    <form method="GET" action="{{ route('applications.all')}}" style="margin-bottom: 10px">
                        <div class="row">
                            <div class="col-md-4">
                                <select id="type" class="form-control" name="topic">
                                    <option selected disabled>Choose Topic</option>

                                    <option value="Health and Medicine" {{ Request::get('topic') == 'Health and Medicine' ? 'selected' : '' }}>Health and Medicine</option>
                                    <option value="Water and Energy" {{ Request::get('topic') == 'Water and Energy' ? 'selected' : '' }}>Water and Energy</option>
                                    <option value="Food and Agriculture" {{ Request::get('topic') == 'Food and Agriculture' ? 'selected' : '' }}>Food and Agriculture</option>
                                    <option value="Transport and Tourism" {{ Request::get('topic') == 'Transport and Tourism' ? 'selected' : '' }}>Transport and Tourism</option>
                                    <option value="Information and Communication Technology" {{ Request::get('topic') == 'Information and Communication Technology' ? 'selected' : '' }}>Information and Communication Technology</option>
                                    <option value="Forest and Environment" {{ Request::get('topic') == 'Forest and Environment' ? 'selected' : '' }}>Forest and Environment</option>
                                    <option value="Disaster Risk Reduction" {{ Request::get('topic') == 'Disaster Risk Reduction' ? 'selected' : '' }}>Disaster Risk Reduction</option>
                                    <option value="Indigenous Knowledge and Technology" {{ Request::get('topic') == 'Indigenous Knowledge and Technology' ? 'selected' : '' }}>Indigenous Knowledge and Technology</option>
                                    <option value="Fundamental Sciences" {{ Request::get('topic') == 'Fundamental Sciences' ? 'selected' : '' }}>Fundamental Sciences</option>
                                    <option value="Applied Sciences" {{ Request::get('topic') == 'Applied Sciences' ? 'selected' : '' }}>Applied Sciences</option>
                                    <option value="Emerging Technology" {{ Request::get('topic') == 'Emerging Technology' ? 'selected' : '' }}>Emerging Technology</option>
                                    <option value="Clean and Green Sciences" {{ Request::get('topic') == 'Clean and Green Sciences' ? 'selected' : '' }}>Clean and Green Sciences</option>
                                    <option value="Economics and Management" {{ Request::get('topic') == 'Economics and Management' ? 'selected' : '' }}>Economics and Management</option>
                                    <option value="Policy" {{ Request::get('topic') == 'Policy' ? 'selected' : '' }}>Policy</option>
                                    <option value="Philosophy" {{ Request::get('topic') == 'Philosophy' ? 'selected' : '' }}>Philosophy</option>
                                    
                                    <option value="Others" {{ Request::get('topic') == 'Others' ? 'selected' : '' }}>Others</option>
                                    <option value="all">All</option>
                                </select>
                            </div>
                            
                            <div class="col-md-4">
                                <select class="form-control" name="theme">
                                    <option selected disabled value="">Choose Theme</option>
                                    
                                    <option value="Science for society" {{ Request::get('theme') == 'Science for society' ? 'selected' : '' }}>Science for society</option>
                                    <option value="Innovation for Prosperity" {{ Request::get('theme') == 'Innovation for Prosperity' ? 'selected' : '' }}>Innovation for Prosperity</option>
                                    <option value="Institutional Reforms" {{ Request::get('theme') == 'Institutional Reforms' ? 'selected' : '' }}>Institutional Reforms</option>
                                    <option value="Investment in Science, Infrastructure development, identification, mobilization, and management of resources" {{ Request::get('theme') == 'Investment in Science, Infrastructure development, identification, mobilization, and management of resources' ? 'selected' : '' }}>Investment in Science, Infrastructure development, identification, mobilization, and management of resources</option>
                                    <option value="Science for Security" {{ Request::get('theme') == 'Science for Security' ? 'selected' : '' }}>Science for Security</option>
                                    <option value="Science, Technology, and Innovation for SDGs" {{ Request::get('theme') == 'Science, Technology, and Innovation for SDGs' ? 'selected' : '' }}>Science, Technology, and Innovation for SDGs</option>
                                    <option value="Youth empowerment for science, technology, and innovation" {{ Request::get('theme') == 'Youth empowerment for science, technology, and innovation' ? 'selected' : '' }}>Youth empowerment for science, technology, and innovation</option>
                                    <option value="Science Diplomacy" {{ Request::get('theme') == 'Science Diplomacy' ? 'selected' : '' }}>Science Diplomacy</option>
                                    <option value="STI Policy" {{ Request::get('theme') == 'STI Policy' ? 'selected' : '' }}>STI Policy</option>
                                    <option value="Academia-Government-Industries partnership for STI" {{ Request::get('theme') == 'Academia-Government-Industries partnership for STI' ? 'selected' : '' }}>Academia-Government-Industries partnership for STI</option>
                                    <option value="Scientific Data Management" {{ Request::get('theme') == 'Scientific Data Management' ? 'selected' : '' }}>Scientific Data Management</option>
                                    <option value="Indigenous and Traditional Knowlegde" {{ Request::get('theme') == 'Indigenous and Traditional Knowlegde' ? 'selected' : '' }}>Indigenous and Traditional Knowlegde</option>
                                    <option value="Intellectual Property Right" {{ Request::get('theme') == 'Intellectual Property Right' ? 'selected' : '' }}>Intellectual Property Right</option>
                                    <option value="Standardization and Quality Assuranced" {{ Request::get('theme') == 'Standardization and Quality Assuranced' ? 'selected' : '' }}>Standardization and Quality Assuranced</option>
                                    <option value="Identification and Prioritization of key areas in STI" {{ Request::get('theme') == 'Identification and Prioritization of key areas in STI' ? 'selected' : '' }}>Identification and Prioritization of key areas in STI</option>
                                    <option value="Scientific tourism" {{ Request::get('theme') == 'Scientific tourism' ? 'selected' : '' }}>Scientific tourism</option>
                                        
                                    <option value="all">All</option>
                                </select>
                            </div>

                            <div class="col-md-4" style="text-align:left;">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <a href="{{ route('applications.all') }}" class="btn btn-warning">Show All</a>
                            </div>
                        </div>
                        
                    </form>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Date of Submission</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($applications as $_applications)
                                <tr>
                                    <td>{{ $loop->iteration + (($applications->currentPage()-1) * $applications->perPage()) }}</td>
                                    <td>{{ $_applications->user->name }}</td>
                                    <td>{{ $_applications->title }}</td>
                                    <td>{{ $_applications->status }}</td>
                                    <td>
                                            {{ date('M j, Y',strtotime($_applications->created_at)) }}
                                    </td>
                                    <td>
                                            <a class="btn btn-outline-primary" title="View Details" href="{{ route('applications.show',$_applications->id) }}"><i class="icon-eye"></i></a>

                                            {{-- <a class="btn btn-outline-warning" title="Update Details" href="{{ route('applications.edit',$_applications->id) }}"><i class="icon-edit"></i></a> --}}
                                        
                                            <form action="{{ route('applications.destroy',$_applications->id) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteNews{{ $_applications->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                            </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $applications->appends($_GET)->links() }}
                            </nav>
                        </div>
                    </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });
   </script>
  
@endsection