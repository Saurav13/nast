@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Application #{{ $_applications->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ url()->previous() }}">Back</a>
                    {{-- <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('applications.edit',$_applications->id) }}">Edit</a> --}}
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <p><b>Name:</b> {{$_applications->user->name}}</p>
                    <p><b>Address:</b> {{$_applications->user->address}} </p>
                    <p><b>Date of Birth:</b> {{$_applications->user->dob}} </p>
                    <p><b>Education:</b> {{$_applications->user->education}} </p>
                    <p><b>Title:</b> {{$_applications->title}} </p>
                    <p><b>Topic:</b> {{$_applications->topic}} </p>
                    <p><b>Themes:</b> @foreach(json_decode($_applications->theme) as $theme){{$theme}} @if(!$loop->last),@endif @endforeach </p>


                    <p><b>Abstract:</b> <a target="_blank" href="/admin/applications/getfile/{{$_applications->id}}/{{$_applications->abstract_file}}">{{$_applications->abstract_file}}</a> </p>
                            (On downloading the file, user will be notified. Status of application will be set to under review)
                    <div class="tab-content px-1 pt-1">
                       
                    </div>

                    {{-- @if($_applications->status != 'accepted' && $_applications->status != 'rejected') --}}
                        <a href="/admin/applications/{{$_applications->id}}/accept" class="btn btn-success">Mark as Accepted</a>
                        <a href="/admin/applications/{{$_applications->id}}/reject" class="btn btn-danger">Mark as Rejected</a>

                    {{-- @endif --}}
                </div>
            </div>
        </section>
    </div>

@endsection
