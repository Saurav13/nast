
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Nast Conference | Register</title>


  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
  <!-- CSS Global Icons -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line/css/simple-line-icons.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">

  <style>
      .invalid-feedback{
        display:inline;
    }
    </style>
  
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
</head>
<style>
    .nav-link{
      font-size:16px;
    }
    
  </style>
  
  
  
  <body>
      <main>
    

  
  
  
      <!-- End Header v1 -->
  
<style>
    .bg-img{
       background:url(/img/formbanner.jpg); background-size:cover; 
    }

    .overlay{
        background-color:#00000050;
        padding:3rem;height:10rem;

    }
    .required
    {
        color: red;
        position: absolute;
        left: 13px;
        top: -6px;
    }
    .form-control{
        border: 1px solid #565656;
    }
</style>
 
    <section class="g-min-height-100vh g-flex-centered g-bg-img-hero g-bg-pos-top-center" style="background-image: url(/img/register.jpg);">
      
        <div class="container g-py-50 g-pos-rel g-z-index-1">
               
            <div class="row justify-content-center u-box-shadow-v24">
                <div class="col-sm-10 col-md-12 col-lg-12" >
                        <div class="text-center mb-0 bg-img" >
                           <div class="overlay">
                            <h2 class="h2 mt-20 g-color-white g-font-weight-600  " style="font-size:33px;">Registration</h2>
                           </div>
                        </div>

                    <div class="g-bg-white rounded g-py-40 g-px-30">
              
                        <!-- Form -->
                        <form class="g-py-15" method="POST" action="http://localhost:8000/register">
                           
                            <input type="hidden" name="_token" value="Y7jVm4NlSbArrvyudOBtrKmKBLkjUoQ4LTRLVO33">
                            <h4 class="h6 g-font-weight-700 g-mb-20">Personal Details:</h4>

                            <div class="row">
                                <div class="col-xs-2 col-sm-2 mb-4">
                                    <input disabled value="{{$user->title}}" id="title" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="first_name" value="" required p="First Name">
                                   
                                </div>
                                <div class="col-xs-3 col-sm-3 mb-4">
                                 
                                    <input disabled value="{{$user->first_name}}" id="first_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="first_name" value="" required p="First Name">

                                </div>
                                <div class="col-xs-3 col-sm-3 mb-4">
                                    
                                        <input disabled value="{{$user->middle_name}}" id="middle_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="middle_name" value=""  p="Middle Name">
    
                                </div>

                                <div class="col-xs-4 col-sm-4 mb-4">
                               
                                    <input disabled value="{{$user->last_name}}" id="last_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="last_name" value="" required p="Last Name">

                                </div>

                                <div class="col-xs-4 col-sm-4 mb-4">
                                   
                                    <input disabled value="{{$user->gender}}" id="" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="last_name" value="" required p="Last Name">
                                    
                                </div>
                                
                              

                                <div class="col-xs-4 col-sm-4 mb-4">
                                   
                                    <input disabled value="{{$user->age}}" id="age" type="number" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="age" min="1" value="" required p="Age">

                                                                    </div>
                                <div class="col-xs-4 col-sm-4 mb-4">
                                 
                                    <input disabled value="{{$user->dob}}" id="dob" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="dob" value="" required p="Date of Birth">

                                                                    </div>

                                 
                            </div>
                            <h4 class="h6 g-font-weight-700 g-mb-20">Address</h4>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                       
                                    <input  disabled value="{{$user->address1}}" id="address1" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="address1" value="" required p="Address 1">

                                                                    </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                     
                                    <input disabled value="{{$user->address2}}" id="address2" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="address2" value="" required p="Address 2">

                                                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                      
                                    <input disabled value="{{$user->city}}" id="city" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="city" value="" required p="City ">

                                      
                                </div>

                                <div class="col-xs-12 col-sm-6 mb-4">
                                  
                                    <input disabled value="{{$user->country}}" id="country" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="country" value="" required p="Country">

                                                                    </div>
                            </div>

                            <h4 class="h6 g-font-weight-700 g-mb-20">Affiliation</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 mb-4">
                                        
                                    <textarea disabled id="affiliation" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="affiliation"  required p="Affiliation">{{$user->affiliation}}</textarea>
                                                                    </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                    
                                    <input disabled value="{{$user->af_address}}" id="af_address" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="af_address" value="" required p="Address">

                                                                    </div>
                                  <div class="col-xs-12 col-sm-6 mb-4">
                                        

                                        <input disabled value="{{$user->designation}}" id="designation" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="af_address" value="" required p="Address">
                                    

                                    
                                                                    </div>
                            </div>

                            <h4 class="h6 g-font-weight-700 g-mb-20">Participation:</h4>

                            <div class="row">
                                    <div class="col-xs-12 col-sm-12 mb-4">
                                           
    
                                            <input disabled value="{{$user->participant_type}}" id="designation" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="af_address" value="" required p="Address">
                                        
    
                                        
                                    </div>
                            </div>
                            <h4 class="h6 g-font-weight-700 g-mb-20">User Account</h4>

                           

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                
                                    <input disabled value="{{$user->email}}" id="email" type="email" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="email" value="" required p="Email">
    
                                                                    </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        
                                    <input disabled value="{{$user->phone}}" id="phone" required type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 " name="phone" value=""  p="Phone Number">
    
                                </div>
                               
                            </div>
                           

                          
                        </form>
                        <!-- End Form -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <button style="position:fixed; right:0; top:0; z-index:999" class="btn btn-md u-btn-darkgray g-font-size-default rounded-0 g-py-10 mr-2" type="button" onclick="javascript:window.print();">
            <i class="g-pos-rel g-top-1 mr-2 icon-education-082 u-line-icon-pro"></i>
            Print
    </button>
<!-- Footer -->
      <!-- End Footer -->
     
       </main>
     
                   
              
         <!-- JS Global Compulsory -->
         <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>
     
         <!-- JS Implementing Plugins -->
         <script src="/frontend-assets/main-assets/assets/vendor/appear.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
     
         <!-- JS Unify -->
         <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
     <script src="/frontend-assets/main-assets/assets/js/components/hs.dropdown.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.scroll-nav.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/helpers/hs.height-calc.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>
     
         <!-- JS Customization -->
         <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>
     
         
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>


         <!-- JS Plugins Init. -->
         <script>
           $(document).on('ready', function () {
             // initialization of carousel
             $('#carousel5').on('click', '.js-thumb', function (e) {
               e.stopPropagation();
               var i = $(this).data('slick-index');
     
                 $('#carousel5').slick('slickGoTo', i);
               
             });
       $('.js-mega-menu').HSMegaMenu();
     
       $.HSCore.components.HSHeader.init($('#js-header'));
     
             $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
               afterOpen: function () {
                 $(this).find('input[type="search"]').focus();
               }
             });
     
             $.HSCore.components.HSCarousel.init('.js-carousel');
     
             // initialization of header
             $.HSCore.components.HSHeader.init($('#js-header'));
             $.HSCore.helpers.HSHamburgers.init('.hamburger');
     
             // initialization of header height offset
             $.HSCore.helpers.HSHeightCalc.init();
     
             // initialization of go to section
             $.HSCore.components.HSGoTo.init('.js-go-to');
           });
     
           $(window).on('load', function() {
             // initialization of HSScrollNav
             $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
               duration: 700
             });
           });
         </script>
       </body>
     </html>
     