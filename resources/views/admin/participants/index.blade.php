@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>

   <div class="content-body">
   

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Participants</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Gender</th>
                                   <th>Age</th>
                                   <th>Dob</th>
                                   <th>Address</th>
                                   <th>Participant Type</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($users as $_users)
                               <tr>
                                   <td>{{ $loop->iteration + (($users->currentPage()-1) * $users->perPage()) }}</td>
                                   <td>{{ $_users->name }}</td>
                                   <td>{{ $_users->email }}</td>
                                   <td>{{ $_users->gender }}</td>
                                   <td>{{ $_users->age }}</td>
                                   <td>{{ $_users->dob }}</td>
                                   <td>{{ $_users->address }}</td>
                                   <td>{{ $_users->participant_type }}</td>

                                  
                                   <td>
                                        <a class="btn btn-outline-primary" title="View Details" href="{{ route('participants.show',$_users->id) }}"><i class="icon-eye"></i></a>

                                      
                                     
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $users->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });
   </script>
  
@endsection