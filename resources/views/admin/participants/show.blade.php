@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Particpant #{{ $_users->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ url()->previous() }}">Back</a>
                    {{-- <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('user.edit',$_users->id) }}">Edit</a> --}}
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <p><b>Name:</b> {{$_users->name}}</p>
                    <p><b>Gender:</b> {{$_users->gender}}</p>
                    <p><b>Age:</b> {{$_users->age}}</p>
                    <p><b>Address:</b> {{$_users->address}} </p>
                    <p><b>Date of Birth:</b> {{$_users->dob}} </p>
                    <p><b>Education:</b> {{$_users->education}} </p>
                    <p><b>Phone Number:</b> {{$_users->phone}} </p>
                    <p><b>Email:</b> {{$_users->email}} </p>
                    <p><b>Participation Type:</b> {{$_users->particpant_type}} </p>

                    
                    <div class="tab-content px-1 pt-1">
                       
                    </div>
                    <a href="/admin/participant/{{$_users->id}}/print" target="_blank" class="btn btn-success">Print Format</a>

                    {{-- @if($_users->status != 'accepted' && $_users->status != 'rejected') --}}
                        
                    {{-- @endif --}}
                </div>
            </div>
        </section>
    </div>

@endsection
