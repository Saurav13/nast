@extends('layouts.admin')

@section('body')

    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Events</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('events.update',$_events->id) }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" >

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input class="form-control{{ $errors->has('title') ? ' border-danger' : '' }}" id="title" type="text" class="form-control" name="title" value="{{ $_events->title }}" required>

                                        @if ($errors->has('title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Location</label>
                                        <input class="form-control{{ $errors->has('location') ? ' border-danger' : '' }}" id="location" type="text" class="form-control" name="location" value="{{ $_events->location }}" required>

                                        @if ($errors->has('location'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('location') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Details</label>
                                        @if ($errors->has('description'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </div>
                                        @endif
                                        <textarea cols="50" rows="20" class="content form-control{{ $errors->has('description') ? ' border-danger' : '' }}" id="en_content" name="description">{{ $_events->description }}</textarea>
                                
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title">Event Date</label>
                                <input class="form-control{{ $errors->has('event_date') ? ' border-danger' : '' }}" id="event_date" type="date" class="form-control" name="event_date" value="{{ $_events->event_date }}" required>

                                @if ($errors->has('event_date'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('event_date') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <input class="form-control{{ $errors->has('image') ? ' border-danger' : '' }}" type="file" placeholder="Photo"  name="image">

                                @if ($errors->has('image'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </div>
                                @endif
                                <br>
                                <label>Current Image:</label><br>
                                <img style="height:5rem" src="/events_images/{{$_events->image}}" >
                            </div>

                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });
   </script>
  
@endsection