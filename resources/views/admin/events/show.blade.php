@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Event #{{ $_events->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('events.index') }}">Back</a>
                    <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('events.edit',$_events->id) }}">Edit</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="english-tab" data-toggle="tab" href="#english" aria-controls="english" aria-expanded="true">English</a>
                        </li>
                    </ul>
                    <div class="tab-content px-1 pt-1">
                        <div role="tabpanel" class="tab-pane fade active in" id="english" aria-labelledby="english-tab" aria-expanded="true">
                                
                            <div id="invoice-template" class="card-block">
                                
                                <div id="invoice-items-details" class="pt-2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-xs-center">
                                                <h2>{{ $_events->title }} </h2>
                                            </div><br>
                                            <div class="text-xs-center">
                                                    <h2>{{ $_events->location }} </h2>
                                            </div><br>
                                            <div class="text-center" style="    text-align: center;">
                                                <img class="img-fluid" src="{{ route('optimize', ['event_images',$_events->image,290,290]) }}" alt="Card image cap">
                                            </div>
                                            <p>{!! $_events->description !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
