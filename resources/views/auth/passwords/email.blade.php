@extends('layouts.app')

@section('title','| Forgot Password')

@section('body')
    <section class="g-height-100vh d-flex align-items-center g-bg-size-cover g-bg-pos-top-center" style="background-image: url(/img/register.jpg);">
        <div class="container g-py-100 g-pos-rel g-z-index-1">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-lg-5">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <header class="text-center mb-4">
                            <h2 class="h2 g-color-black g-font-weight-600">{{ __('Reset Password') }}</h2>
                            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>We will send you a link to reset your password.</span></h6>
                        </header>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <!-- Form -->
                        <form class="g-py-15" method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="mb-4">
                                <input id="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required type="email" placeholder="Email Address">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            
                            <div class="mt-4 text-center">
                                <button class="btn btn-md u-btn-primary rounded g-pt-13 g-px-25" type="submit">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </form>
                        <!-- End Form -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
