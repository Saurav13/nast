@extends('layouts.app')

@section('title','| Reset Password')

@section('body')
    <section class="g-height-100vh d-flex align-items-center g-bg-size-cover g-bg-pos-top-center" style="background-image: url(/img/register.jpg);">
        <div class="container g-py-100 g-pos-rel g-z-index-1">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-lg-5">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <header class="text-center mb-4">
                            <h2 class="h2 g-color-black g-font-weight-600">Reset Password</h2>
                        </header>
                
                        <!-- Form -->
                        <form class="g-py-15" method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 mb-4">
                                    <input id="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required type="email" placeholder="Email Address">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
            
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                    <input id="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required type="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
            
                                <div class="col-xs-12 col-sm-6 mb-4">
                                    <input id="password-confirm" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="password" placeholder="Confirm Password" name="password_confirmation" required>
                                </div>
                            </div>
            
                            <div class="row justify-content-between">
                                <div class="col align-self-center text-center">
                                <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">{{ __('Reset Password') }}</button>
                                </div>
                            </div>
                        </form>
                        <!-- End Form -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
