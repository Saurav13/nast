@extends('layouts.app')

@section('title','| Register')

@section('css')

<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
@endsection

@section('body')

<style>
    .bg-img{
       background:url(/img/formbanner.jpg); background-size:cover; 
    }

    .overlay{
        background-color:#00000050;
        padding:3rem;height:10rem;

    }
    .required
    {
        color: red;
        position: absolute;
        left: 13px;
        top: -6px;
    }
    .form-control{
        border: 1px solid #565656;
    }
</style>
    <section class="g-min-height-100vh g-flex-centered g-bg-img-hero g-bg-pos-top-center" style="background-image: url(/img/register.jpg);">
        <div class="container g-py-50 g-pos-rel g-z-index-1">
            <div class="row justify-content-center u-box-shadow-v24">
                <div class="col-sm-10 col-md-12 col-lg-12" >
                        <div class="text-center mb-0 bg-img" >
                           <div class="overlay">
                            <h2 class="h2 mt-20 g-color-white g-font-weight-600  " style="font-size:33px;">Registration</h2>
                           </div>
                        </div>

                    <div class="g-bg-white rounded g-py-40 g-px-30">
              
                        <!-- Form -->
                        <form class="g-py-15" method="POST" action="{{ route('register') }}">
                            {{csrf_field()}}
                            <h4 class="h6 g-font-weight-700 g-mb-20">Personal Details:</h4>

                            <div class="row">
                                <div class="col-xs-2 col-sm-2 mb-4">
                                    <span class="required">*</span>
                                    <select style="height:53px;" name="title" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('title') ? ' is-invalid' : '' }}" required>
                                        <option selected>Mr.</option>
                                        <option>Ms.</option>
                                        <option>Dr.</option> 
                                        <option>Er.</option>                                        
                                        <option>Prof.</option>
                                    </select>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-3 mb-4">
                                        <span class="required">*</span>

                                    <input id="first_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required placeholder="First Name">

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-3 mb-4">
                                    
                                        <input id="middle_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}"  placeholder="Middle Name">
    
                                        @if ($errors->has('middle_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('middle_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                <div class="col-xs-4 col-sm-4 mb-4">
                                        <span class="required">*</span>

                                    <input id="last_name" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required placeholder="Last Name">

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-xs-4 col-sm-4 mb-4">
                                    <span class="required">*</span>

                                    <select placeholder="Select Gender" name="gender" style="height:53px;" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('gender') ? ' is-invalid' : '' }}" required>
                                        <option value="">Gender</option>
                                        
                                        <option>Male</option>
                                        <option>Female</option>
                                        <option>Others</option>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('gemder') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                              

                                <div class="col-xs-4 col-sm-4 mb-4">
                                    <span class="required">*</span>

                                    <input id="age" type="number" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" min="1" value="{{ old('age') }}" required placeholder="Age">

                                    @if ($errors->has('age'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('age') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-4 col-sm-4 mb-4">
                                        <span class="required">*</span>

                                    <input id="dob" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob" value="{{ old('dob') }}" required placeholder="Date of Birth">

                                    @if ($errors->has('dob'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('dob') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                 
                            </div>
                            <h4 class="h6 g-font-weight-700 g-mb-20">Address</h4>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="address1" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('address1') ? ' is-invalid' : '' }}" name="address1" value="{{ old('address1') }}" required placeholder="Address 1">

                                    @if ($errors->has('address1'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address1') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="address2" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('address2') ? ' is-invalid' : '' }}" name="address2" value="{{ old('address2') }}" required placeholder="Address 2">

                                    @if ($errors->has('address2'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address2') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="city" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required placeholder="City ">

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif  
                                </div>

                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="country" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}" required placeholder="Country">

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <h4 class="h6 g-font-weight-700 g-mb-20">Affiliation</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 mb-4">
                                        <span class="required">*</span>
                                   
                                    <textarea id="affiliation" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('affiliation') ? ' is-invalid' : '' }}" name="affiliation"  required placeholder="Affiliation">{{ old('affiliation') }}</textarea>
                                    @if ($errors->has('affiliation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('affiliation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="af_address" type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('af_address') ? ' is-invalid' : '' }}" name="af_address" value="{{ old('af_address') }}" required placeholder="Address">

                                    @if ($errors->has('af_address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('af_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                  <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>


                                    <select class="form-control rounded-0 {{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" style="height:53px;" required>
                                        <option {{ !old('designation') ? 'selected' : '' }} disabled value="">Designation</option>
                                        <option value="Student" {{ old('designation') == 'Student' ? 'selected' : '' }}>Student</option>
                                        <option value="Teacher" {{ old('designation') == 'Teacher' ? 'selected' : '' }}>Teacher</option>
                                        <option value="Professor" {{ old('designation') == 'Professor' ? 'selected' : '' }}>Professor</option>
                                        <option value="Researcher" {{ old('designation') == 'Researcher' ? 'selected' : '' }}>Researcher</option>
                                        <option value="Entrepreneurs" {{ old('designation') == 'Entrepreneurs' ? 'selected' : '' }}>Entrepreneurs</option>
                                        <option value="Others" {{ old('designation') == 'Others' ? 'selected' : '' }}>Others</option>
                                    </select>

                                    
                                    @if ($errors->has('designation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('designation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <h4 class="h6 g-font-weight-700 g-mb-20">Participation:</h4>

                            <div class="row">
                                    <div class="col-xs-12 col-sm-12 mb-4">
                                            <span class="required">*</span>
    
    
                                        <select class="form-control rounded-0 {{ $errors->has('participant_type') ? ' is-invalid' : '' }}" name="participant_type" style="height:53px;" required>
                                            <option {{ !old('participant_type') ? 'selected' : '' }} disabled value="">Select One</option>
                                            <option value="Participant" {{ old('participant_type') == 'Participant' ? 'selected' : '' }}>I am here for participation only.</option>
                                            <option value="PaperSubmission" {{ old('participant_type') == 'PaperSubmission' ? 'selected' : '' }}>I am here for participation and paper presentation</option>
                                            
                                        </select>
    
                                        
                                        @if ($errors->has('participant_type'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('participant_type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                            </div>
                            <h4 class="h6 g-font-weight-700 g-mb-20">User Account</h4>

                           

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="email" type="email" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="phone" required type="text" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}"  placeholder="Phone Number">
    
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input id="password" type="password" class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required placeholder="Password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-xs-12 col-sm-6 mb-4">
                                        <span class="required">*</span>

                                    <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-brd-primary--hover rounded g-py-15 g-px-15" type="password" name="password_confirmation"  required placeholder="Confirm Password">
                                </div>
                            </div>

                            <div class="row justify-content-between mb-5">
                                {{-- <div class="col align-self-center">
                                    <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa g-rounded-2" data-check-icon="&#xf00c"></i>
                                    </div>
                                    I accept the <a href="#!">Terms and Conditions</a>
                                    </label>
                                </div> --}}
                                <div class="col align-self-center text-right">
                                    <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Register</button>
                                </div>
                            </div>
                        </form>
                        <!-- End Form -->

                        <footer class="text-center">
                            <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Already have an account? <a class="g-font-weight-600" href="page-signup7.html">signin</a>
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>


@endsection
