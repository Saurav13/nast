@extends('layouts.app')

@section('title','| Verify your Email')

@section('body')

<section class="g-min-height-100vh g-flex-centered g-bg-img-hero g-bg-pos-top-center" style="background-image: url(/img/register.jpg);">
    <div class="container g-py-100 g-pos-rel g-z-index-1">
        <div class="row justify-content-center u-box-shadow-v24">
            <div class="col-sm-10 col-md-9 col-lg-9">
                <div class="g-bg-white rounded g-py-40 g-px-30">
                    <header class="text-center mb-4">
                        <h2 class="h2 g-color-black g-font-weight-600">{{ __('Verify Your Email Address') }}</h2>
                    </header>
                    
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                        
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
