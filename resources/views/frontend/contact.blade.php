@extends('layouts.app')
@section('title','Contact Us')

@section('css')
<link rel="stylesheet" href="/frontend/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="/frontend/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link rel="stylesheet" href="/frontend/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
<style>
  .invalid-feedback{
      display: block;
  }
  .h3{
    font-size:24px;
  }
  .box{
    border: 2px solid #0099da;
    padding: 1rem;
    border-radius: 11px;

  }
</style>
@endsection

@section('body')
<div class="row align-items-stretch">
    <div class="col-lg-12 g-mb-30">
        <!-- Article -->
        <article class="text-center g-color-white g-overflow-hidden">
            <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                
                </div>
            </div>
        </article>
        <!-- End Article -->
    </div>
  </div>
    <!-- Promo Block -->
    <section >
       
            <!-- Promo Block Content -->
     
          <!-- End Promo Block -->
          
          <section class="clearfix g-brd-bottom g-brd-gray-light-v4">
            <div class="text-center">
                <h3 class=" g-color-black g-font-weight-800 text-uppercase mb-4">Secretariat</h2>
              
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="g-mx-40">
                  <p class="h3 g-color-black  mb-2">Ms. Luna Vajra, NAST (Coordinator) </p>
                  <p class="h3 g-color-black g-font-weight-600  mb-2"><u>Members</u></p>

                  <p class="h3 g-color-black  mb-2">Mr. Khim Prasad Panthi, NYC  </p>
                  <p class="h3 g-color-black mb-2">Dr. Lok Ranjan Bhatta, NAST  </p>
                  <p class="h3 g-color-black  mb-2">Dr. Jay Kant Raut, NAST   </p>
                  <p class="h3 g-color-black mb-2">Dr. Iswor Bajracharya, NAST  </p>
                  <p class="h3 g-color-black mb-2">Dr. Ram Chandra Poudel, NAST </p>
                  <p class="h3 g-color-black mb-2">Mr. Bimal Kumar Raut, TC-TU </p>
                  <p class="h3 g-color-black mb-2">Mr. Bishwonath Bhattarai, NAST (Member Secretary) </p>
                </div>
    
              </div>
              <div class="col-md-6">
                  <div class="g-mx-40 box" >
                      <p class="h3 g-color-black   mb-2"> Nepal Academy of Science and Technology (NAST)</p>
                     <p class="h3 g-color-black   mb-2"> Khumaltar, Lalitpur</p>
                     <p class="h3 g-color-black   mb-2"> GPO Box: 3323</p>


                     <p class="h3 g-color-black  mb-2"> Phone: +977-1-5547268, +977-1-5547715</p>
                     <p class="h3 g-color-black   mb-2"> Mobile: +977-9813097093</p>
                      <p class="h3 g-color-black   mb-2">E-Mail: <span class="g-color-primary">info@nastconference.org</span></p>
                     <p class="h3 g-color-black   mb-2"> Website: <span class="g-color-primary">www.nastconference.org</span> </p>

                  </div>
              </div>
            </div>
              <!-- End Icons Block -->
            </section>
          </section>
        </div>
        <div class="g-mt-20">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14135.91836888491!2d85.3276755!3d27.6561031!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16a8449f116b5fe!2sNepal+Academy+of+Science+and+Technology+(NAST)!5e0!3m2!1sen!2snp!4v1565347887672!5m2!1sen!2snp" style="width:100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
          <!-- End Contact Form -->
      
@endsection

@section('js')
<script src="/frontend/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
@endsection