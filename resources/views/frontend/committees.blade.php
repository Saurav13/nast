@extends('layouts.app')


@section('body')
<style>
.desc *{
    color:black;
  }
  .h3{
    /* font-weight: 500; */
  }
  </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12 g-mb-30">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>

        <div class="g-mx-40 g-mb-40">
            <div class="">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="h3 g-color-black g-font-weight-600 text-uppercase mb-2">Steering Committee: </h2><br>
                            <p class="h3 g-color-black  mb-2" >Dr. Sunil Babu Shrestha, Vice Chancellor, NAST (Coordinator)
  
                            </p>
                            

                            <p class="h3 g-color-black  mb-2" >Mr. Madhab Prasad Dhungel, Vice President, NYC<br>
                                
                                    
                            </p>
                    <p class="h3 g-color-black g-font-weight-600  mb-2"><u>Members</u></p>
                        <p class="h3 g-color-black  mb-2" >
                            Prof. Dr. Anjana Singh, Academician, NAST<br>
                        </p>
                            <p class="h3 g-color-black  mb-2" >
                                Mr. Chhabi Rijal, Joint Secretary, Office of the Prime Minister<br>
                            </p>
                            <p class="h3 g-color-black  mb-2" >

                                Mr. Ganesh Prasad Pandey, Joint Secretary, Ministry of Youth and Sports<br>
                                
                            </p>
                            <p class="h3 g-color-black  mb-2" >Mr. Surendra Subedi , Joint Secretary, Ministry of Education, Science and Technology

                            </p>
                            <p class="h3 g-color-black  mb-2" >

                                Dr. Kiran Rupakheti, Joint Secretary, National Planning Commission<br>
                                
                            </p>
                            <p class="h3 g-color-black  mb-2" >

                                     Representative, Ministry of Finance<br>
                                     
                                 </p>
     
                                
                            <p class="h3 g-color-black  mb-2" >

                                     Representative, Ministry of Foreign Affairs<br>
                                     
                                 </p>
                           
                            <p class="h3 g-color-black  mb-2" >Dr. Mahesh K. Adhikari, NAST (Member Secretary)

                              
                            </p>
                         
                        </div>
                        <div class="col-md-4">  
                                <h2 class="h3 g-color-black g-font-weight-600 text-uppercase mb-2">Organizing Committee: </h2><br>
                                <h2 class="h3 g-color-black  mb-2" >Dr. Mahesh K. Adhikari, Secretary, NAST (Coordinator)
                                </h2>
                                <p class="h3 g-color-black g-font-weight-600  mb-2"><u>Members</u></p>
                                <h2 class="h3 g-color-black  mb-2" >Prof. Dr. Ram Prasad Khatiwada, TU <br> 
                                 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Prof. Dr. Kanhaiya Jha, KU<br> 
                                </h2>
                               
                                <h2 class="h3 g-color-black  mb-2" >Mr. Surendra Subedi, MoEST 

                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Mr. Ganesh Pandey, MoYS 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Dr. Babu Ram Dhungana, NYC<br> 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Dr. Bhanu Bhakta Neupane, CDC-TU<br> 
                                </h2>
                              
                                <h2 class="h3 g-color-black  mb-2" >Mr. Khim Prasad Panthi, NYC <br> 
                                </h2>
                               
                                
                                
                              
                              
                                <h2 class="h3 g-color-black  mb-2" >Ms. Jaishree Sijipati, NAST <br> 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Dr. Rabindra Prashad Dhakal, NAST<br> </h2>
                                    <h2 class="h3 g-color-black  mb-2" >Mr. Niranjan Acharya, NAST<br> 
                                    </h2>
                                <h2 class="h3 g-color-black  mb-2" >Ms. Neesha Rana, NAST <br> 

                                </h2>

                                <h2 class="h3 g-color-black  mb-2" >Ms. Sashi Hamal, NAST<br> 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Ms. Luna Vajra, NAST  <br>
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Ms. Anita Shrestha, NAST<br> 
                                </h2>
                                <h2 class="h3 g-color-black  mb-2" >Chief, Promotion and Publicity Division , NAST (Member Secretary)<br> 
                                </h2>
            
                        </div>
                        <div class="col-md-4">  
                           

                    <h2 class="h3 g-color-black g-font-weight-600 text-uppercase mb-2">Scientific Committee: </h2><br>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Suresh Kumar Dhungel, NAST (Coordinator)</h2>
                    <p class="h3 g-color-black g-font-weight-600 mb-2"><u>Members</u></p>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Achyut Adhikari, CDC-TU </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Anjana Giri, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Arnico Panday, ICIMOD </h2>
                    <h2 class="h3 g-color-black  mb-2" >Ms. Bhawani Oli, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Bhoj Raj Pant, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Bimala Devkota, NAST</h2>
                    <h2 class="h3 g-color-black  mb-2" >Mr. Bishow Nath Yadav, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr.Budha Ram Saha, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Deegendra Khadka, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Deepa Dhital, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr.Deepashree Rawal,NAST</h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Dipendra Das Mulmi, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Er. Dipendra Gautam, IOE </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Gan B. Bajracharya, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Ishwor Bajracharya, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Jay Kant Raut, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Jyoti Maharjan, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Er. Kabita Pandey, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Kishor Pandey, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Lok Ranjan Bhatta, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Er. Manoj Saha, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Meghnath Dhimal, NHRC </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Naresh Manandhar, KMC </h2>
                    <h2 class="h3 g-color-black  mb-2" >Mr. Pawan Neupane, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Ram Chandra Poudel, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Rosha Ranjit, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Er. Roshan Pandey, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Sajjan Lal Shyaula, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Er. Subash Poudel, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr.Sandesh Bhattarai, NAST  </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Tirtha Raj Ghimire, NAST </h2>
                    <h2 class="h3 g-color-black  mb-2" >Dr. Tista Prasai, NAST (Member Secretary)  </h2>

                        </div>
                    </div>


               



                        
                            
            </div>


        </div>
        
          
           
  </div>

@endsection