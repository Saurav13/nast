@extends('layouts.app')



@section('title','| Home')


@section('css')
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">

<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">

<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">

@endsection

@section('body')
      <!-- Main Banner -->
{{-- <section id="home" class="js-carousel g-overflow-hidden g-max-height-100vh"
      data-autoplay="true"
      data-infinite="true"
      data-speed="15000"
      data-pagi-classes="u-carousel-indicators-v29 container text-center text-uppercase g-absolute-centered--x g-bottom-50 g-line-height-1_2 g-font-size-12 g-color-white g-brd-white-opacity-0_2">
    <div class="js-slide"
        data-title="About us">
      <!-- Promo Block -->
      <div class="g-flex-centered g-height-100vh g-min-height-500--md g-bg-cover g-bg-pos-center g-bg-img-hero g-bg-black-opacity-0_2--after" style="background-image: url(/img/main1.jpg);">
        <div class="container text-center g-z-index-1">
          <h2 class="text-uppercase g-font-weight-700 g-font-size-22 g-font-size-36--md g-color-white g-mb-20">Youth 
            <span class="g-color-primary">National</span> 
            Conference
          </h2>
          <p class="g-hidden-xs-down g-max-width-645 g-color-white-opacity-0_9 mx-auto g-mb-35">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key. Now that we've aligned the details, it's time to get things mapped out and organized.</p>
          <a class="btn btn-lg u-btn-primary g-font-weight-700 g-font-size-12 text-uppercase g-rounded-50 g-px-40 g-py-15" href="/apply">Apply Now</a>
        </div>
      </div>
      <!-- End Promo Block -->
    </div>

    <div class="js-slide"
        data-title="Experience">
      <!-- Promo Block -->
      <div class="g-flex-centered g-height-100vh g-min-height-500--md g-bg-cover g-bg-pos-center g-bg-img-hero g-bg-black-opacity-0_2--after" style="background-image: url(/img/main2.jpg);">
        <div class="container text-center g-z-index-1">
          <h2 class="text-uppercase g-font-weight-700 g-font-size-22 g-font-size-36--md g-color-white g-mb-20">Science for
          
            <span class="g-color-primary">Society</span> : <br>Innovation For
            <span class="g-color-primary">Prosperity</span> 
          </h2>
          <p class="g-hidden-xs-down g-max-width-645 g-color-white-opacity-0_9 mx-auto g-mb-35">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key. Now that we've aligned the details, it's time to get things mapped out and organized.</p>
          <a class="btn btn-lg u-btn-primary g-font-weight-700 g-font-size-12 text-uppercase g-rounded-50 g-px-40 g-py-15" href="#!">Apply Now</a>
        </div>
      </div>
      <!-- End Promo Block -->
    </div>

    <div class="js-slide"
        data-title="Services">
      <!-- Promo Block -->
      <div class="g-flex-centered g-height-100vh g-min-height-500--md g-bg-cover g-bg-pos-center g-bg-img-hero g-bg-black-opacity-0_2--after" style="background-image: url(/img/main3.jpg);">
        <div class="container text-center g-z-index-1">
          <h2 class="text-uppercase g-font-weight-700 g-font-size-22 g-font-size-36--md g-color-white g-mb-20">We are a accounting firm unify
            <br> providing
            <span class="g-color-primary">tax</span> and
            <span class="g-color-primary">accounting</span> services
          </h2>
          <p class="g-hidden-xs-down g-max-width-645 g-color-white-opacity-0_9 mx-auto g-mb-35">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key. Now that we've aligned the details, it's time to get things mapped out and organized.</p>
          <a class="btn btn-lg u-btn-primary g-font-weight-700 g-font-size-12 text-uppercase g-rounded-50 g-px-40 g-py-15" href="#!">View Presentation</a>
        </div>
      </div>
      <!-- End Promo Block -->
    </div>

    <div class="js-slide"
        data-title="Who we work">
      <!-- Promo Block -->
      <div class="g-flex-centered g-height-100vh g-min-height-500--md g-bg-cover g-bg-pos-center g-bg-img-hero g-bg-black-opacity-0_2--after" style="background-image: url(/img/main4.jpg);">
        <div class="container text-center g-z-index-1">
          <h2 class="text-uppercase g-font-weight-700 g-font-size-22 g-font-size-36--md g-color-white g-mb-20">We are a accounting firm unify
            <br> providing
            <span class="g-color-primary">tax</span> and
            <span class="g-color-primary">accounting</span> services
          </h2>
          <p class="g-hidden-xs-down g-max-width-645 g-color-white-opacity-0_9 mx-auto g-mb-35">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key. Now that we've aligned the details, it's time to get things mapped out and organized.</p>
          <a class="btn btn-lg u-btn-primary g-font-weight-700 g-font-size-12 text-uppercase g-rounded-50 g-px-40 g-py-15" href="#!">View Presentation</a>
        </div>
      </div>
      <!-- End Promo Block -->
    </div>
</section> --}}
<!-- End Main Banner -->
  <!-- Promo Block - Parallax -->
  <section class="g-pos-rel">
    <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}' data-calc-target="">
      <div class="divimage dzsparallaxer--target w-100  g-bg-img-hero " style="height: 100%; background:url(/img/bg3.jpg); background-size:cover"></div>

      <div class=" text-center g-z-index-1 g-pt-30 g-pb-50" style="    margin: 1rem 10%;" >
        <div style=" padding:2rem; border-radius:10px; border:2px solid white;background:#0099da40"> 
        <h3 class="g-color-white g-font-weight-700 g-font-size-35 g-font-size-35--md text-uppercase mb-0" data-animation="fadeInUp" data-animation-delay="800" data-animation-duration="1500">
              International Youth Conference 
            </h3>
            <h3 class="g-color-white g-font-size-20 g-font-weight-700 mb-0" data-animation="fadeInUp" data-animation-delay="800" data-animation-duration="1500">On</h3>
            <h3 class="g-color-white g-font-weight-700 g-font-size-35 g-font-size-35--md text-uppercase g-mb-40" data-animation="fadeInUp" data-animation-delay="800" data-animation-duration="1500">
                Science, Technology and Innovation
              </h3>
        <div class="g-mb-25" data-animation="fadeInUp" data-animation-delay="500" data-animation-duration="1500">
          <h2 class="h4 d-inline-block g-font-weight-600 text-uppercase g-pb-5" style="font-size:28px;color:#b93834">“ Research and Innovation for Prosperity ”</h2>
        </div>

        <div class="g-mb-5" data-animation="fadeInUp" data-animation-delay="500" data-animation-duration="1500">
          <h2 class="h4 d-inline-block g-font-weight-600 g-pb-5" style="color:yellow">October 21-23, 2019</h2>
        </div>
        <h3 style="font-size:1.5rem" class="g-color-white g-font-weight-700 text-uppercase g-mb-40" data-animation="fadeInUp" data-animation-delay="800" data-animation-duration="1500">
          Kathmandu, Nepal
        </h3>
      
        <div class="g-mb-40" data-animation="fadeInUp" data-animation-delay="1100" data-animation-duration="1500">
        <div class="row">
            <div class="col-md-4">
                <img src="/img/logo.png" style="height:12rem"/>
  
            </div>
          <div class="col-md-4">
            <img src="/img/GON.png" style="height:12rem"/>
          </div>
          <div class="col-md-4">
              <img src="/img/NYC.png" style="height:12rem"/>

          </div>
          
        </div>
        </div>
        <div data-animation="fadeInUp" data-animation-delay="1100" data-animation-duration="1500">
            <div class="row">
                <div class="col-md-4">
                  <h3 class="g-color-white g-font-size-20 g-font-weight-700 mb-0" >Abstract Submission Deadline:</h3>
                  <h3 class="g-font-size-20 g-font-weight-700 mb-0" style="color:yellow">25<sup>th</sup> September, 2019</h3>
      
                </div>
              <div class="col-md-4">
                  <h3 class="g-color-white g-font-size-20 g-font-weight-700 mb-0" > Notification Date:</h3>
                  <h3 class=" g-font-size-20 g-font-weight-700 mb-0" style="color:yellow">1<sup>st</sup> October, 2019</h3>
    
              </div>
              <div class="col-md-4">
                  <h3 class="g-color-white g-font-size-20 g-font-weight-700 mb-0" >Confirmation Date:</h3>
                  <h3 class=" g-font-size-20 g-font-weight-700 mb-0" style="color:yellow">15<sup>th</sup> October, 2019</h3>
    
    
              </div>
              
            </div>
        </div>
        <br>
        <div class="text-center" data-animation="fadeInUp" data-animation-delay="1100" data-animation-duration="1500">
          <a class="btn btn-primary btn-lg"  href="/brochure">View Brochure</a>
          {{-- <a class="btn btn-primary btn-lg"  href="/profile"></a> --}}

        </div>
      </div>
      </div>
    </div>
  </section>
  <!-- End Promo Block -->





@endsection

@section('js')
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>

<script src="/frontend-assets/main-assets/assets/js/components/hs.onscroll-animation.js"></script>

<script>
  $('document').ready(function(){
    $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

  })
</script>
@endsection