@extends('layouts.app')


@section('body')
<style>
  .desc *{
    color:black;
  }
  
  .guidelines_p{
    font-size: 20px;
    color: black;
    text-align: justify;
  }

  .guidelines_ul li{
    font-size: 20px;
    color: black;
  }
  
  .guidelines > p{
    font-size: 20px;
  }
  .p1,.p2,.p3,.p4{
    font-size: 20px;

  }
  .s1,.s2,.s3,.s4{
    font-size: 20px;

  }
  </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>

        
          
           
  </div>
  <section style="width:100%; height:6rem; background:#0099da">
      <div class="text-center">
          <h2 class="h3 g-color-white text-uppercase mb-2" style="padding-top: 1.8rem;">Conference Guidelines</h2>
      </div>
  </section>
  <section class="g-px-40 g-pt-50 g-pb-50">
    
    <div class="row justify-content-center g-mb-60">
      <div class="col-lg-12">
        <!-- Heading -->
       
      <div style="text-align: justify guidelines">
          {{-- <p class="mb-10" style="font-size:18px">Please review the guidelines carefully before submitting your abstract.</p> --}}
          <p class="p1"><span class="s1"><strong>Guidelines for Poster Preparation</strong></span></p>
          <p class="p2"><span class="s2">Each poster <strong>presenter</strong> will be provided with a 3' x 5' poster board area and mounting pins. The board will indicate the poster number in upper right or left corner.&nbsp;Presenters are responsible for mounting their posters the morning of their presentation and removing them as soon as the session ends.&nbsp; </span></p>
          <p class="p2"><span class="s2"><strong>Posters should include:</strong></span></p>
          <p class="p3"><span class="s2">Title</span></p>
          <p class="p3"><span class="s2">Author name (s)</span></p>
          <p class="p3"><span class="s2">Affiliation</span></p>
          <p class="p3"><span class="s2">Abstract</span></p>
          <p class="p3"><span class="s2">Background (brief introduction and objectives)</span></p>
          <p class="p3"><span class="s2">Material &amp; Method (concise or in flow chart)</span></p>
          <p class="p3"><span class="s2">Results (analytical and in diagrams)</span></p>
          <p class="p3"><span class="s2">Conclusion (precise and in bullets) </span></p>
          <p class="p3"><span class="s2">Acknowledgement (Optional)</span></p>
          <p class="p4"><span class="s2">Reference (Max. 5)</span></p>
          <p class="p5"><span class="s2">Limit the text to about one-fourth of the poster space, and use "visuals" (graphs, photographs, schematics, maps, etc.) to tell your "story."</span><span class="s3"><br /></span></p>
          <p class="p6"><span class="s2">Each poster must include text in a large enough font (~20 point font) to be read easily by attendees from a distance of 4 to 5 feet or more. Lettering on illustrations should be large and legible. Photographs should be a minimum of 5 x 7 inches. </span></p>
          <p class="p4"><span class="s2">Avoid overcrowding figures and cramming too many numbers into tables. Legends and titles should accompany all figures, tables, photographs, etc. in order to allow their immediate identification.<br /></span></p>
          <p class="p4"><span class="s2">No commercial activities or any advertising may be displayed on the posters. Non-compliance with this rule will result in the poster being removed.<br /></span></p>
          <p class="p4"><span class="s2">Requests for extra space to accommodate models or equipment should be made at the time of abstract submission and are subject to approval.<br /></span></p>
          <p class="p4"><span class="s2">Posters will be arranged by topic on one of three meeting days (Monday, Tuesday, or Wednesday) and remain up the full day. Approximately two hours each of those days is set aside for poster presentations when no other sessions are scheduled. The authors need not be present the entire time but if not, should post the hours when they will be at their poster.</span></p>
          <table class="t1" cellspacing="0" cellpadding="0">
          <tbody>
          <tr>
          <td class="td1" valign="middle">
          <p class="p7">&nbsp;</p>
          </td>
          </tr>
          </tbody>
          </table>
 
            {{-- <a href="/guidelines.docx" download class="btn btn-primary">Download Guidelines</a> --}}
        </div>
        <!-- End Heading -->
      </div>
    </div>
  </section>

  @endsection