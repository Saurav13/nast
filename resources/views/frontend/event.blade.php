@extends('layouts.app')


@section('body')
<style>
  .desc *{
    color:black;
  }
  span,ol,em,p{
    font-size:20px!important;
  }
 
  </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12 ">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>
          
           
  </div>
  <section style="width:100%; height:6rem; background:#0099da">
      <div class="text-center">
          <h2 class="h3 g-color-white g-font-size-25 g-font-weight-700 text-uppercase mt-10 mb-2" style="padding-top: 1.8rem;">{{$event->title}}</h2>
          {{-- <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div> --}}
          {{-- <p class="mb-0">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p> --}}
            
        </div>
  </section>
  <section class="container g-pb-50">
    
        <div class="row justify-content-center ">
          <div class="col-lg-12">
            <!-- Heading -->
            
            <!-- End Heading -->
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12 g-mb-50">

            <img class="img-fluid" src="/events_images/{{$event->image}}" alt="Image Description">
          </div>
          <div class="col-lg-12">
        <a href="/guidelines" class="pull-right btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-25 g-py-13 mb-0">View Guidelines</a>

        <a href="/apply" class="pull-right btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-25 g-py-13 mb-0 mr-1" >Apply Now</a>

            <div class="mb-5 desc" style="text-align:justify">

            {!!$event->description!!}

            </div>
        <a href="/guidelines" class="pull-right btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-25 g-py-13 mb-0">View Guidelines</a>

        <a href="/apply" class="pull-right btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-25 g-py-13 mb-0 mr-1">Apply Now</a>

          </div>
        </div>
  </section>
@endsection