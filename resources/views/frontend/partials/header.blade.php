

  <!DOCTYPE html>
<html lang="en">

<head>
    <title>Nast Conference @yield('title')</title>


  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
  <!-- CSS Global Icons -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line/css/simple-line-icons.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">

  <style>
      .invalid-feedback{
        display:inline;
    }
    </style>
  @yield('css')
</head>
