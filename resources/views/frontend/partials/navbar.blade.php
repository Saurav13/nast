
<style>
    .nav-link{
      font-size:16px;
    }
    
  </style>
  
  
  
  <body>
      <main>
    
    
    
        <!-- Header -->
        <header id="js-header" class="u-header u-header--static">
          <div class="u-header__section u-header__section--light g-bg-primary g-transition-0_3 g-py-10">
            <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
              <div class="g-mx-60">
                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                  <span class="hamburger hamburger--slider">
                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                  </span>
                  </span>
                </button>
                <!-- End Responsive Toggle Button -->
    
                <div style="display:flex;margin:0.5rem 0rem">
                <!-- Navigation -->
                <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg" id="navBar">
                  <ul class="navbar-nav  g-pos-rel g-font-weight-600 ml-auto">
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/" class="nav-link g-py-7 g-px-0 g-color-white">Home</a>
                    </li>
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/conference" class="nav-link g-py-7 g-px-0 g-color-white">About Conference</a>
                    </li>
                    {{-- <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/guidelines" class="nav-link g-py-7 g-px-0 g-color-white">Guidelines</a>
                    </li> --}}
                    <li class="nav-item hs-has-sub-menu menu_hover">
                        <a id="nav-link--pages--about" class="nav-link g-color-white" href="#" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--pages--about">Guidelines</a>
  
                        <!-- Submenu (level 2) -->
                        <ul class="hs-sub-menu list-unstyled g-bg-primary u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2 animated" id="nav-submenu--pages--about" aria-labelledby="nav-link--pages--about" style="display: none;">
                          <li class="dropdown-item g-font-weight-600 g-px-10 g-py-7 menu_hover ">
                            <a class="nav-link  g-color-white" href="/guidelines">Conference Guidelines</a>
                          </li>
                          <li class="dropdown-item g-font-weight-600 g-px-10 g-py-7 menu_hover">
                            <a class="nav-link g-color-white" href="/poster-guidelines">Poster Guidelines</a>
                          </li>
                          <li class="dropdown-item g-font-weight-600 g-px-10 g-py-7 menu_hover">
                            <a class="nav-link g-color-white" target="_blank" href="/Abstract.pdf">Abstract Guidelines</a>
                          </li>
                      
               
                        </ul>
                        <!-- End Submenu (level 2) -->
                      </li>
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/immigration" class="nav-link g-py-7 g-px-0 g-color-white">Immigration</a>
                    </li>
            
  
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/register" class="nav-link g-py-7 g-px-0 g-color-white"  >Registration</a>
                    </li>
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/apply" class="nav-link g-py-7 g-px-0 g-color-white"> Submission</a>
                    </li>
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/committees" class="nav-link g-py-7 g-px-0 g-color-white">Committees </a>
                    </li>
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/contact" class="nav-link g-py-7 g-px-0 g-color-white">Secretariat </a>
                    </li>
                    @guest
                    @else
                    <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                      <a href="/profile" class="nav-link g-py-7 g-px-0 g-color-white">My Profile </a>
                    </li>
                    @endif
                  </ul>
                </div>
                <!-- End Navigation -->
    
                <div class="pull-right d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg">
                    @guest
                    <a style="border:1px solid" class="btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-40 g-py-15" href="/login">Log in</a>
                    
                  @else
                    <a style="border:1px solid" class="btn  btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-40 g-py-15" href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out</a>
                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  @endif
                </div>
              </div>
              </div>
            </nav>
          </div>
        </header>
  
  
  
  {{-- <body style="background:#f9ffe4;">
    <main>
      <!-- Header v1 -->
      <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance"
              data-header-fix-moment="100">
        <div class="u-header__section u-header__section--dark g-bg-black-opacity-0_3 g-transition-0_3 g-py-10"
             data-header-fix-moment-exclude="g-bg-black-opacity-0_3 g-py-10"
             data-header-fix-moment-classes="g-bg-black-opacity-0_8 g-py-10">
          <nav class="navbar navbar-expand-lg py-10">
            <div class="container g-pos-rel">
              <div id="navBar" class="collapse navbar-collapse w-100" data-mobile-scroll-hide="true">
                <!-- Navigation -->
                <div class="navbar-collapse align-items-center flex-sm-row">
                  <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-font-weight-700 g-font-size-13 g-py-20 g-py-0--lg">
                    <li class="nav-item g-mr-10--lg g-mr-15--xl g-my-7 g-mb-0--lg ">
                      <a href="/" class="nav-link p-0">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item g-mx-10--lg g-mx-15--xl g-my-7 g-mb-0--lg">
                      <a href="/conference" class="nav-link p-0">About Conference</a>
                    </li>
  
                    <li class="nav-item g-mx-10--lg g-mx-15--xl g-my-7 g-mb-0--lg">
                        <a href="/guidelines" class="nav-link p-0">Guidelines</a>
                    </li>
                    <li class="nav-item g-mx-10--lg g-mx-15--xl g-my-7 g-mb-0--lg">
                        <a href="/register" class="nav-link p-0">Registration</a>
                      </li>
                    <li class="nav-item g-mx-10--lg g-mx-15--xl g-my-7 g-mb-0--lg">
                      <a href="/apply" class="nav-link p-0">Submission</a>
                    </li>
                    <li class="nav-item g-mx-10--lg g-mx-15--xl g-my-7 g-mb-0--lg">
                        <a href="/committees" class="nav-link p-0">Committees</a>
                      </li>
                    <li class="nav-item g-ml-10--lg g-ml-15--xl g-my-7 g-mb-0--lg">
                      <a href="/contact" class="nav-link p-0">Secetariat</a>
                    </li>
                  </ul>
                </div>
                <!-- End Navigation -->
                @guest
                  <a class="btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-40 g-py-15" href="/login">Log in</a>
                  
                @else
                  <a class="btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 g-rounded-30 g-px-40 g-py-15" href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out</a>
                  <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                @endif
              </div>
  
              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-5 g-right-0" type="button"
                aria-label="Toggle navigation"
                aria-expanded="false"
                aria-controls="navBar"
                data-toggle="collapse"
                data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
            </div>
          </nav>
        </div>
      </header> --}}
      <!-- End Header v1 -->
  