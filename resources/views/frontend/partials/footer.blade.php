
      {{-- <footer>
        <div class="g-color-gray-dark-v5 g-theme-bg-gray-dark-v3">
          <div class="container">
            <div class="text-center text-md-left g-brd-top g-brd-gray-dark-v2 g-py-30">
              <div class="row">
                <div class="col-md-12 col-lg-4 g-mb-25 g-mb-0--lg">
                  <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">About Kathmandu</h2>
                  <p class="g-mb-20 g-font-size-16">
                      Kathmandu is the capital of Nepal and an ancient city, famous for its temples and Buddhist stupas. The average temperature during mid October lies between12 to 27 degree Celsius. There are regular international flights connecting Kathmandu to Europe, USA, Japan and other major cities of the globe.
                  </p>
                </div>
                <div class="col-md-12 col-lg-4 g-mb-25 g-mb-0--lg">
                    <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Visa to Nepal</h2>
                    <p class="g-mb-20 g-font-size-16">
                        Visa can be obtained from Nepalese Embassy/Consulate offices abroad or on arrival in Nepal. For further information please visit: <a href="http://www.welcomenepal.com" target="_blank"> http://www.welcomenepal.com</a>

                            
                    </p>
                    <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Local Hospitality</h2>
                    <p class="g-mb-20 g-font-size-16">
                        Accommodation in hotels or guest house can be arranged for the participants on request and on payment of the expenses.
                      <p>
                  </div>



                <div class="col-md-12 col-lg-4">
                  <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Top Link</h2>
                  <ul class="list-unstyled g-mb-30 g-mb-0--md">
                    <li class="g-mb-10">
                      <a class="g-color-gray-dark-v5 g-font-size-20" href="/conference">About Conference</a>
                    </li>
                    <li class="g-mb-10">
                      <a class="g-color-gray-dark-v5 g-font-size-20" href="/guidelines">Guidelines</a>
                    </li>
                    <li class="g-mb-10">
                      <a class="g-color-gray-dark-v5 g-font-size-20" href="/register">Registration</a>
                    </li>
                    <li class="g-mb-10">
                      <a class="g-color-gray-dark-v5 g-font-size-20" href="/apply">Submission</a>
                    </li>
                  </ul>
                </div>

              </div>
            </div>

            <div class="text-center text-md-left g-brd-top g-brd-gray-dark-v2 g-py-40">
              <div class="row">
                <div class="col-md-6 d-flex align-items-center g-mb-15 g-mb-0--md">
                  <p class="w-100 mb-0">
                    2019 All right reserved. <a class="g-font-weight-600 g-color-white" href="http://www.nast.gov.np/"> NAST Nepal.</a>
                    
                    Powered by
                    <a class="g-font-weight-600 g-color-white" href="https://www.incubeweb.com/">Incube Technologies</a>
                  </p>
                </div>

                <div class="col-md-6">
                  <ul class="list-inline float-md-right mb-0">
                    <li class="list-inline-item g-mr-10">
                      <a class="u-icon-v2 g-width-35 g-height-35 g-font-size-16 g-color-gray-light-v1 g-color-white--hover g-bg-primary--hover g-brd-gray-dark-v2 g-brd-primary--hover g-rounded-5" href="#!"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="list-inline-item g-mr-10">
                      <a class="u-icon-v2 g-width-35 g-height-35 g-font-size-16 g-color-gray-light-v1 g-color-white--hover g-bg-primary--hover g-brd-gray-dark-v2 g-brd-primary--hover g-rounded-5" href="#!"><i class="fa fa-pinterest-p"></i></a>
                    </li>
                    <li class="list-inline-item g-mr-10">
                      <a class="u-icon-v2 g-width-35 g-height-35 g-font-size-16 g-color-gray-light-v1 g-color-white--hover g-bg-primary--hover g-brd-gray-dark-v2 g-brd-primary--hover g-rounded-5" href="#!"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a class="u-icon-v2 g-width-35 g-height-35 g-font-size-16 g-color-gray-light-v1 g-color-white--hover g-bg-primary--hover g-brd-gray-dark-v2 g-brd-primary--hover g-rounded-5" href="#!"><i class="fa fa-linkedin"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer> --}}

     <!-- Footer -->
     <div id="contacts-section" class="g-bg-primary g-color-white-opacity-0_8 g-py-60">
        <div class="container">
          <div class="row">
            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
               <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">About Kathmandu</h2>
               <p class="g-mb-20 g-font-size-16" style="text-align: justify">
                   Kathmandu, the capital of Nepal, well renowned as city of temples, is famous for its history, art and deviant religions. The <a href="http://www.mfd.gov.np/" style="color:white" target="_blank"> average temperature</a> during mid October lies between 12 to 27 degree Celsius. There are regular international flights connecting Kathmandu to major cities of the globe.
               </p>
            </div>
            <!-- End Footer Content -->
     
            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
               <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Visa to Nepal</h2>
               <p class="g-mb-20 g-font-size-16" style="text-align: justify" >
                   Visa can be obtained from Nepalese Embassy/Consulate offices abroad or on arrival in Nepal. For further information please visit: <br><a href="http://www.welcomenepal.com" style="color:white" target="_blank"> http://www.welcomenepal.com</a>
     
                       
               </p>
               <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Local Hospitality</h2>
               <p class="g-mb-20 g-font-size-16" style="text-align: justify">
                   Accommodation in hotels or guest house can be arranged for the participants on request and on payment of the expenses.
               <p>
            </div>
            <!-- End Footer Content -->
     
            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
               <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Quick Link </h2>
             
     
              <nav class="text-uppercase1">
                <ul class="list-unstyled g-mt-minus-10 mb-0">
                   <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/brochure">Brochure</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/conference">About Conference</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/guidelines">Guidelines</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                   <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/immigration">Immigration</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/register">Registration</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/apply">Submission</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/committees">Committees</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                  <li class="g-pos-rel g-py-10">
                    <h4 class="h6 g-pr-20 mb-0">
                     <a class="g-color-white-opacity-0_8 g-color-white--hover g-font-size-16" href="/contact">Secretariat</a>
                     <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                   </h4>
                  </li>
                </ul>
              </nav>
            </div>
            <!-- End Footer Content -->
     
            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6">
              
               <h2 class="text-uppercase g-font-weight-700 g-font-size-default g-color-white g-mb-15">Contact Us </h2>
     
              <address class="g-bg-no-repeat mb-0" style="background-image: url(../../../assets/img/maps/map2.png);">
            <!-- Location -->
            <div class="d-flex g-mb-20">
              <div class="g-mr-10">
                <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                  <i class="fa fa-map-marker"></i>
                </span>
              </div>
              <p class="mb-0 g-font-size-16" >Kathmandu, Nepal</p>
            </div>
            <!-- End Location -->
     
            <!-- Phone -->
            <div class="d-flex g-mb-20">
              <div class="g-mr-10">
                <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
              <p class="mb-0 g-font-size-16" >01-5547268<br> 01-5547715<br>+977-9813097093</p>
            </div>
            <!-- End Phone -->
     
            <!-- Email and Website -->
            <div class="d-flex g-mb-20">
              <div class="g-mr-10">
                <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                  <i class="fa fa-globe"></i>
                </span>
              </div>
              <p class="mb-0">
                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="mailto:info@htmlstream.com">info@nastconference.org</a>
             </p>
            </div>
             <div class="d-flex g-mb-20">
              <div class="g-mr-10">
                <a href="https://www.facebook.com/NAST-Conference-102027421177016/" target="_blank" class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/ConferenceNast" target="_blank" class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                  <i class="fa fa-twitter"></i>
                </a>
              </div>
            
            </div>
            <!-- End Email and Website -->
          </address>
            </div>
            <!-- End Footer Content -->
          </div>
        </div>
      </div>
      <!-- End Footer -->
     
      <!-- Copyright Footer -->
      <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
              <div class="d-lg-flex">
                <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2019 &copy; All Rights Reserved. Powered By <strong><a href="https://www.incubeweb.com/" target="_blank">InCube</a></strong></small>
                
              </div>
            </div>
     
            <div class="col-md-4 align-self-center">
            
            </div>
          </div>
        </div>
      </footer>
      <!-- End Copyright Footer -->
      <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
       "bottom": 15,
       "right": 15
     }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
     </main>
     @if($errors->count()>0)
     <div class="container">
        <div class="row">
          <div>
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; bottom: 20px; right: 10px; animation-iteration-count: 1;">
              <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <h4 class="h5">
                <i class="fa fa-check-circle"></i>
                Error
              </h4>
              {{$errors}}          
          </div>
      </div>
      </div>
    </div>
    @endif
         @if (Session::has('success'))
         <div class="container">
             <div class="row">
               <div>
                 <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-success alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; bottom: 20px; right: 10px; animation-iteration-count: 1;">
                   <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                   </button>
                   <h4 class="h5">
                     <i class="fa fa-check-circle"></i>
                     Success
                   </h4>
                   <p>{{Session::get('success')}}</p>              
               </div>
           </div>
           </div>
         </div>
         @endif
     
         @if (Session::has('error'))
         <div class="container">
             <div class="row">
               <div>
                 <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; bottom: 20px; right: 10px; animation-iteration-count: 1;">
                   <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                   </button>
                   <h4 class="h5">
                     <i class="fa fa-check-circle"></i>
                     Oops
                   </h4>
                   <p>{{Session::get('error')}}</p>              
               </div>
           </div>
           </div>
         </div>
         @endif
     
         <!-- JS Global Compulsory -->
         <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>
     
         <!-- JS Implementing Plugins -->
         <script src="/frontend-assets/main-assets/assets/vendor/appear.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
         <script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
     
         <!-- JS Unify -->
         <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
     <script src="/frontend-assets/main-assets/assets/js/components/hs.dropdown.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.scroll-nav.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/helpers/hs.height-calc.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>
         <script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>
     
         <!-- JS Customization -->
         <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>
     
         @yield('js')
         <!-- JS Plugins Init. -->
         <script>
           $(document).on('ready', function () {
             // initialization of carousel
             $('#carousel5').on('click', '.js-thumb', function (e) {
               e.stopPropagation();
               var i = $(this).data('slick-index');
     
                 $('#carousel5').slick('slickGoTo', i);
               
             });
       $('.js-mega-menu').HSMegaMenu();
     
       $.HSCore.components.HSHeader.init($('#js-header'));
     
             $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
               afterOpen: function () {
                 $(this).find('input[type="search"]').focus();
               }
             });
     
             $.HSCore.components.HSCarousel.init('.js-carousel');
     
             // initialization of header
             $.HSCore.components.HSHeader.init($('#js-header'));
             $.HSCore.helpers.HSHamburgers.init('.hamburger');
     
             // initialization of header height offset
             $.HSCore.helpers.HSHeightCalc.init();
     
             // initialization of go to section
             $.HSCore.components.HSGoTo.init('.js-go-to');
           });
     
           $(window).on('load', function() {
             // initialization of HSScrollNav
             $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
               duration: 700
             });
           });
         </script>
       </body>
     </html>
     