@extends('layouts.app')
@section('title','Facilities')

@section('body')
    
    <div id="news-section" class="g-bg-secondary g-py-100">
        <div class="container">
            <!-- Heading -->
            <div class="g-max-width-550 text-center mx-auto g-mb-70">
            <h2 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-13 g-letter-spacing-2 mb-4">News Blog</h2>
            <h2 class="h3 mb-5">Read the latest news and blogs.</h2>
            </div>
            <!-- End Heading -->
            @foreach($blogs as $blog)
            <div class="row g-mx-minus-25 g-mb-50">
            <div class="col-lg-6 g-px-25 g-mb-50">
                <!-- Blog Grid Modern Blocks -->
                <article class="row align-items-stretch no-gutters u-shadow-v29 g-transition-0_3">
                <div class="col-sm-6 g-bg-white g-rounded-left-5">
                    <div class="g-pa-35">
                    <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                        <li class="list-inline-item mr-0">{{$blog->author}}</li>
                        <li class="list-inline-item mx-2">&#183;</li>
                        <li class="list-inline-item">{{$blog->publish_date}}</li>
                    </ul>

                    <h2 class="h5 g-color-black g-font-weight-600 mb-4">
                        <a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="#!">{{$blog->title}}</a>
                    </h2>
                    <p class="g-color-gray-dark-v4 g-line-height-1_8 mb-4">{!!$blog->description!!}</p>

                    </div>
                </div>
                <div class="col-sm-6 g-bg-size-cover g-bg-pos-center g-min-height-300 g-rounded-right-5" data-bg-img-src="/blogs_image/{{$blog->image}}"></div>
                </article>
                <!-- End Blog Grid Modern Blocks -->
            </div>
            </div>
            @endforeach
        {{$blogs->links()}}
        </div>
    </div>

@endsection
