@extends('layouts.app')


@section('body')

<style>
    .simple_p{
        /* color: black;  */
         font-size: 16px;
    }
    .label{
        font-size:16px;
        color:black;
    }
    </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12 g-mb-30">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>
          
           
  </div>
  <section class="container g-pt-10 g-pb-50">
        <div class="row justify-content-center g-mb-0">
          <div class="col-lg-7">
            <!-- Heading -->
            {{-- <div class="text-center">
              <h2 class="h3 g-color-black text-uppercase mb-2">Apply to Youth Conference</h2>
              <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
              <p class="mb-0">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>
                
            </div> --}}
            <!-- End Heading -->
          </div>
        </div>
        {{-- {{$application}} --}}
        <div class="">
            @if($application==null)
                @include('frontend.application_partials.abstract_form')
            @elseif($application->status=='submitted' || $application->status=='under review')
                @include('frontend.application_partials.submitted')
            @elseif($application->status=='accepted')
                @include('frontend.application_partials.accepted')
            @elseif($application->status=='rejected')
                @include('frontend.application_partials.rejected')
            @elseif($application->status=='paid')
                @include('frontend.application_partials.paid')
            @elseif($application->status=='full_paper_submitted')
                @include('frontend.application_partials.final')
            @endif
        </div>
        
  </section>
@endsection


@section('js')
      <!-- JS Implementing Plugins -->
      <script  src="/frontend-assets/main-assets/assets/vendor/jquery.filer/js/jquery.filer.min.js"></script>

      <!-- JS Unify -->
      <script  src="/frontend-assets/main-assets/assets/js/helpers/hs.focus-state.js"></script>
      <script  src="/frontend-assets/main-assets/assets/js/components/hs.file-attachement.js"></script>

      <!-- JS Plugins Init. -->
      <script >
        $(document).on('ready', function () {
              // initialization of forms
              $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
              $.HSCore.helpers.HSFocusState.init();
            });
      </script>

<script>
        $(document).ready(function(){
            $('#topic').on('change',function(){
                if($('#topic').val()=='Others')
                {
                    $('#other_value').removeAttr('hidden');
                }
                else{
                    $('#other_value').attr('hidden','true');
    
                }
            })
        })
    </script>
    

@endsection