@extends('layouts.app')

@section('title',$album->name)

@section('body')

        
        
</div>
<section class="g-my-100">
  <div class="container">
    <div class="g-max-width-550 text-center mx-auto g-mb-40">
        <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-18 g-letter-spacing-2 mb-4"><a href="/albums">Albums </a>| {{$album->name}}</h1>
    </div>
    <div class="row">
        @foreach($images as $image)
        <div class="col-md-4 g-mb-30">
            <a class="js-fancybox d-block u-block-hover" href="javascript:;" data-fancybox="lightbox-gallery--03" data-src="/gallery/{{$image->image}}" data-speed="350" data-caption="Lightbox Gallery">
            <img class="img-fluid u-block-hover__main--mover-right" src="/gallery/{{$image->image}}" alt="Image Description">
            </a>
        </div>
        @endforeach
       
    </div>
     <!-- Pagination -->
    <nav class="g-mt-50 text-center" aria-label="Page Navigation">
        {{ $images->appends(request()->except('page'))->links('frontend.partials.pagination') }}
    </nav>
    <!-- End Pagination -->

    </div>
</section>

@endsection
