@extends('layouts.app')


@section('body')
<style>
  .desc *{
    color:black;
  }
  
  .guidelines_p{
    font-size: 20px;
    color: black;
    text-align: justify;
  }
  .guidelines_ul li{
    font-size: 20px;
    color: black;
  }
  
  </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>

        
          
           
  </div>
  <section style="width:100%; height:6rem; background:#0099da">
      <div class="text-center">
          <h2 class="h3 g-color-white text-uppercase mb-2" style="padding-top: 1.8rem;">Conference Guidelines</h2>
      </div>
  </section>
  <section class="g-px-40 g-pt-50 g-pb-50">
    
    <div class="row justify-content-center g-mb-60">
      <div class="col-lg-12">
        <!-- Heading -->
       
      <div style="text-align: justify">
          {{-- <p class="mb-10" style="font-size:18px">Please review the guidelines carefully before submitting your abstract.</p> --}}
         
            <h2></h2>
            <p class="guidelines_p">Before participating or submitting an <a href="/guidelines.docx" download>extended abstract</a> in the conference, participants should carefully read and follow the guidelines outlined below. Failure to comply with guidelines may lead to rejection of your participation in the conference.</p>
 
            <ul class="guidelines_ul">
   <li>Participants must <a href="/register">register</a> by the registration deadline. (ie 25th September, 2019)</li>
<li>  If you are accepted to participate, you must confirm your registration with payment by the deadline in order to be included in the conference program. Participants who fail to make payment for the conference by the payment deadline will be withdrawn from the program.</li>
 <li>
With the registration, participants are requested to follow the necessary steps to participate e.g.  managing the fund for participation as needed, applying for a visa on a timely basis if needed, keeping your schedule free of conflicting commitments, and fully preparing for the conference.
 </li>
 <li>
To withdraw the participation due to unavoidable circumstances, participants must notify the Conference Secretariat as soon as possible. 
 </li>
 <li>
Once the program is set, there will not be any scheduling changes. </li>


  <li>
The final and full programme of the conference will be made available online for all participants via conference website or on the abstract book distributed at the registration desk 
  </li>
  <li>
    The working language of the conference will be English and Nepali.
  </li>
  <li>
    For updated information please visit conference website <a href="https://www.nastconference.org/">(www.nastconference.org)</a>. 
  </li>
            {{-- <a href="/guidelines.docx" download class="btn btn-primary">Download Guidelines</a> --}}
        </div>
        <!-- End Heading -->
      </div>
    </div>
  </section>

  @endsection