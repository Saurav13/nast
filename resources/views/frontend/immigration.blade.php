@extends('layouts.app')


@section('body')
<style>
  .desc *{
    color:black;
  }
  .guidelines_p{
    font-size: 20px;
    color: black;
    text-align: justify;
  }
  .guidelines_ul li{
    font-size: 20px;
    color: black;
  }
  
  </style>
  <div class="row align-items-stretch">
        <div class="col-lg-12 g-mb-30">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>

        
          
           
  </div>

  <section class="container g-pt-50 g-pb-50">
    
    <div class="row justify-content-center g-mb-60">
      <div class="col-lg-12">
        <!-- Heading -->
        <div class="text-center">
          <h2 class="h3 g-color-black text-uppercase mb-2">Immigration</h2>
        </div>
      <div>
          {{-- <p class="mb-10" style="font-size:18px">Please review the guidelines carefully before submitting your abstract.</p> --}}
         
            <h2></h2>
            <p class="guidelines_p">
                    Basically, visitors to Nepal must obtain a visa from concerned Nepalese diplomatic missions unless they come from one of the <a target="_blank" href="http://nepalimmigration.gov.np/page/visa-exemption">visa exempt countries</a>. 
                    On default, you will receive an invitation letter from the IYCSTI 2019 organizer via email and in general, such an invitation letter is sufficient for visa application. If you need an original printed invitation letter with an official stamp, please complete the visa form (*.doc or *.pdf) and send it to the conference secretary. 
            </p>
 
          

            {{-- <a href="/guidelines.docx" download class="btn btn-primary">Download Guidelines</a> --}}
        </div>
        <!-- End Heading -->
      </div>
    </div>
  </section>

  @endsection