
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Nast Conference</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">

  <!-- Page Style -->
  <style type="text/css">
    @media print {
        * { -webkit-print-color-adjust: exact; }

        .u-header,
        .footer,
        footer,
        .breadcrumbs,
        .btn {
          display: none;
        }

        .row > [class*="col-"] {
          float: left;
        }

        .col-md-3,
        .col-md-6 {
          width: 33%;
        }

        .col-lg-2 {
          width: 24%;
        }

        .g-bg-primary {
          color: #000 !important;
        }

        .g-font-size-default {
          font-size: 11px !important;
        }

        .h4,
        .h5 {
          font-size: 14px !important;
        }

        .g-pa-15 {
          padding: 10px !important;
        }

        .py-4 {
          padding-top: 10px !important;
          padding-bottom: 10px !important;
        }

        .g-py-15 {
          padding-top: 5px !important;
          padding-bottom: 5px !important;
        }

        .g-pt-100 {
          padding-top: 30px !important;
        }

        .g-pb-30 {
          padding-bottom: 0 !important;
        }

        .g-mb-30,
        .g-mb-40 {
          margin-bottom: 20px !important;
        }

        .my-2 {
          margin-top: 0;
          margin-bottom: 0;
        }

        table tr th {
          font-size: 13px !important;
        }
      }
  </style>
</head>

<body>
  <main>






 <br>
 <h4 class="text-center g-font-weight-600">Invoice for {{$payment->user->name}}</h4><br>


 <section class="container g-mb-40">
        <div class="row justify-content-end no-gutters">
          <div class="col-sm-4 col-md-3 col-lg-2 mb-1">
            <div class="g-bg-gray-light-v5 text-center g-pa-15">
              <h3 class="h6 g-color-black g-font-weight-600 text-uppercase"># Invoice No</h3>
            <p class="g-font-size-13 mb-0">{{$payment->id}}</p>
            </div>
          </div>
  
          <div class="col-sm-4 col-md-3 col-lg-2 mb-1">
            <div class="g-bg-gray-light-v5 text-center g-pa-15">
              <h3 class="h6 g-color-black g-font-weight-600 text-uppercase"># Invoice Date</h3>
              <p class="g-font-size-13 mb-0">{{$payment->created_at->format('d/m/Y')}}</p>
            </div>
          </div>
  
        </div>
      </section>
    <!-- Table Striped Rows -->
    <section class="container g-pb-70">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead class="g-color-white g-bg-primary text-center text-uppercase">
            <tr>
              <th class="g-brd-top-none g-font-weight-500 g-py-15">Ref</th>
              <th class="g-brd-top-none g-font-weight-500 text-left g-py-15">Item description</th>
              <th class="g-brd-top-none g-font-weight-500 g-py-15">Quantity</th>
              <th class="g-brd-top-none g-font-weight-500 g-py-15">Unit price</th>
              <th class="g-brd-top-none g-font-weight-500 g-py-15">Total</th>
            </tr>
          </thead>

          <tbody class="text-center">
            <tr>
              <td class="g-width-70 g-color-gray-dark-v4 g-font-weight-600 g-py-15">01</td>
              <td class="g-max-width-300 text-left g-py-15">
                <h4 class="g-color-gray-dark-v4 g-font-weight-700 g-font-size-16">NAST Conference Participation/Paper Submission</h4>
              
              </td>
              <td class="g-color-gray-dark-v4 g-font-weight-600 g-py-15">1</td>
            <td class="g-color-gray-dark-v4 g-font-weight-600 g-py-15">{{$payment->user_type=="Nepalese"?'Rs. 2000':'$500 (USD)'}}</td>
              <td class="g-color-gray-dark-v4 g-font-weight-600 g-py-15">{{$payment->user_type=="Nepalese"?'Rs. 2000':'$500 (USD)'}}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <!-- Subtotal -->
      <div class="g-bg-gray-light-v5 mb-2">
        <div class="row justify-content-end">
          <div class="col-lg-5">
            <ul class="list-unstyled g-font-weight-600 text-uppercase py-4 g-pr-50 mb-0">
              <li class="text-right my-1">
                <h4 class="d-inline-block h6 text-left g-font-weight-600 g-min-width-110 mb-0">Subtotal</h4>
                <span class="text-left mx-5">:</span>
                <span class="d-inline-block g-min-width-65">{{$payment->user_type=="Nepalese"?'Rs. 2000':'$500 (USD)'}}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- End Subtotal -->

      <!-- Total -->
      <div class="row justify-content-between">
        <div class="col-md-4 align-self-center g-hidden-sm-down g-mb-30">
          <ul class="list-unstyled mb-0">
          <li class="my-1"><span class="g-font-weight-600">Address:</span> {{$payment->user->address}}</li>
            <li class="my-1"><span class="g-font-weight-600">Email:</span> {{$payment->user->email}}</li>
            <li class="my-1"><span class="g-font-weight-600">Phone:</span> {{$payment->user->phone}}</li>
           </ul>
        </div>

        <div class="col-md-7 col-lg-4 align-self-center g-mb-30">
          <div class="g-bg-gray-light-v5 g-color-black g-font-weight-600 text-right text-uppercase py-4 g-pr-50 mb-3">
            <h4 class="d-inline-block h6 text-left g-font-weight-600 g-min-width-110 mb-0">Grand Total</h4>
            <span class="text-left mx-5">:</span>
            <span class="d-inline-block g-min-width-65">{{$payment->user_type=="Nepalese"?'Rs. 2000':'$500 (USD)'}}</span>
          </div>
          <div class="text-right">
            <button class="btn btn-md u-btn-darkgray g-font-size-default rounded-0 g-py-10 mr-2" type="button" onclick="javascript:window.print();">
              <i class="g-pos-rel g-top-1 mr-2 icon-education-082 u-line-icon-pro"></i>
              Print
            </button>
          </div>
        </div>
      </div>
      <!-- End Total -->
    </section>
    <!-- End Table Striped Rows -->



  </main>



  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.js/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Implementing Plugins -->
  <script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.tabs.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>

  <!-- JS Customization -->
  <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(document).on('ready', function () {
        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
      });

      $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 991
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
  </script>



  <div id="copyModal" class="text-left modal-demo g-bg-white g-color-black g-pa-20" style="display: none;"></div>

  <!-- CSS -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/chosen/chosen.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/prism/themes/prism.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/style-switcher/vendor/spectrum/spectrum.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/style-switcher/vendor/spectrum/themes/sp-dark.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/style-switcher/style-switcher.css">
  <!-- End CSS -->

  <!-- Scripts -->
  <script src="/frontend-assets/main-assets/assets/vendor/chosen/chosen.jquery.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/image-select/src/ImageSelect.jquery.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/clipboard/dist/clipboard.min.js"></script>

  <!-- Prism -->
  <script src="/frontend-assets/main-assets/assets/vendor/prism/prism.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/components/prism-markup.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/components/prism-css.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/components/prism-clike.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/components/prism-javascript.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/plugins/toolbar/prism-toolbar.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/prism/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js"></script>
  <!-- End Prism -->

  <script src="/frontend-assets/main-assets/assets/js/components/hs.scrollbar.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.select.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.modal-window.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.markup-copy.js"></script>

  <script src="/frontend-assets/main-assets/assets/style-switcher/vendor/cookiejs/jquery.cookie.js"></script>
  <script src="/frontend-assets/main-assets/assets/style-switcher/vendor/spectrum/spectrum.js"></script>
  <script src="/frontend-assets/main-assets/assets/style-switcher/style-switcher.js"></script>
  <!-- End Scripts -->
  <!-- End Style Switcher -->

</body>

</html>
