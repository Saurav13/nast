@extends('layouts.app')

@section('body')

<div class="row align-items-stretch">
        <div class="col-lg-12 g-mb-30">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                    
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>
          
           
  </div>

  @if($application)

  <div class="container">
        <div class="text-center">
                      
                <h2 class="h3 g-color-black text-uppercase mb-2">Your current Application: </h2>
                <br>

        </div>

    <ul class="list-unstyled g-mb-60">
        <!-- Events Item -->
        <li class=" rounded g-px-50 g-py-30 mb-4" style="background:#0099da40"> 
          <div class="row align-items-lg-center">
            <div class="col-md-3 col-lg-2 g-mb-30 g-mb-0--lg">
              <div class="d-flex align-items-center mb-3">
                <span class="g-color-black g-font-size-50 g-line-height-1 mr-3">{{$application->created_at->format('d')}}</span>
                <div class="g-color-gray-dark-v4 text-center g-line-height-1_4">
                  <span class="d-block g-color-black">{{$application->created_at->format('M')}}</span>
                  <span class="d-block g-color-black">{{$application->created_at->format('Y')}}</span>
                </div>
              </div>
              </div>
            <div class="col-md-9 col-lg-8 g-mb-30 g-mb-0--lg">
              <h3 class="h5 g-color-black g-font-weight-500 mb-1">{{$application->event->title}}</h3>
              <p style="margin-bottom:0px" class="g-color-black">{{$application->event->event_date}}</p>
              <p class="g-color-black">{{$application->event->location}}</p>

              <p class="g-color-black" style="margin-bottom:0px">Paper Title: {{$application->title}}</p>

              <p class="g-color-black">Application Status: {{$application->status}}</p>

            </div>
            <div class="col-lg-2">
              <a class="btn btn-primary g-brd-2 g-brd-gray-light-v4 g-brd-primary--hover g-color-white g-color-white--hover g-bg-primary--hover g-rounded-30 g-py-10" href="/apply">View Details</a>
            </div>
          </div>
        </li>
    </ul>

      <!-- End More Events List -->
</div>
@else
<div class="container text-center">
    <div class="text-center">
                      
        <h2 class="h3 g-color-black  mb-2">Welcome {{Auth::user()->name}} </h2>
        <br><br><br>
        <div class="row">
            <div class="col-lg-4 g-mb-60">
              <!-- Icon Blocks -->
              <div class="u-shadow-v19 text-center g-rounded-5 g-px-30 g-pb-30" style="background:#0099da40">
                <span class="u-icon-v3 g-bg-primary g-color-white g-pull-50x-up g-rounded-5 g-mb-5">
                  <i class="icon-education-087 u-line-icon-pro"></i>
                </span>
                <h3 class="h5 g-color-black g-mb-10 g-font-size-24 g-font-weight-600">Brochure</h3>
                <p class="g-color-black g-mb-20">Download brochure for more information about conference.</p>
                <a class="btn btn-primary g-font-weight-600 g-font-size-12 g-text-underline--none--hover text-uppercase" href="#">Download</a>
              </div>
              <!-- End Icon Blocks -->
            </div>
          
            <div class="col-lg-4 g-mb-60">
              <!-- Icon Blocks -->
              <div class="u-shadow-v19 text-center g-rounded-5 g-px-30 g-pb-30" style="background:#0099da40">
                <span class="u-icon-v3 g-bg-primary g-color-white g-pull-50x-up g-rounded-5 g-mb-5">
                  <i class="icon-finance-009 u-line-icon-pro"></i>
                </span>
                <h3 class="h5 g-color-black g-mb-10 g-font-size-24 g-font-weight-600">Guidelines</h3>
                <p class="g-color-black g-mb-20">Please view our guidelines before applying to the conference.</p>
                <a class="btn btn-primary g-font-weight-600 g-font-size-12 g-text-underline--none--hover text-uppercase" href="/guidelines">View Guidelines</a>
              </div>
              <!-- End Icon Blocks -->
            </div>
          
            <div class="col-lg-4 g-mb-60">
              <!-- Icon Blocks -->
              <div class="u-shadow-v19 text-center g-rounded-5 g-px-30 g-pb-30 " style="background:#0099da40">
                <span class="u-icon-v3 g-bg-primary g-color-white g-pull-50x-up g-rounded-5 g-mb-5">
                  <i class="icon-finance-256 u-line-icon-pro"></i>
                </span>
                <h3 class="h5 g-color-black g-mb-10 g-font-size-24 g-font-weight-600">Apply to Conference</h3>
                <p class="g-color-black g-mb-20">Have you looked at our guidelines?. If yes, then you can apply now.</p>
                <a  class="btn btn-primary  g-font-weight-600 g-font-size-12 g-text-underline--none--hover text-uppercase" href="/apply">Apply Now</a>
              </div>
              <!-- End Icon Blocks -->
            </div>
          </div>
          <!-- End Icon Blocks -->
    </div>
  {{-- <p><a class="btn btn-primary btn-lg" href="/apply">Apply to conference</a></p> --}}

</div>

@endif

<div class="container">
                     
    <h2 class="h3 g-color-black  mb-2 text-center">Payment </h2>
@if(App\User::findOrFail(Auth::user()->id)->paid_status=="unpaid" || App\User::findOrFail(Auth::user()->id)->paid_status=="rejected")

  <p style="font-size:18px; margin-bottom:1px" class="text-center"> You can make payment either with <strong>E-sewa</strong> or deposit amount in our bank and upload the voucher image or deposit statement.</p>
 <div style="padding: 1rem; border: 2px solid #0099da; border-radius: 12px;">
  <p style="font-size:18px; margin-bottom:1px">Bank Details:</p>
  <p style="font-size:18px; margin-bottom:1px">Account Name:  NAST Conference</p>
  <p style="font-size:18px; margin-bottom:1px">Account Number: 0211-8010-4665-3016</p>
  <p style="font-size:18px; margin-bottom:1px">Bank Name: Agriculture Development Bank Ltd.</p>
  <p style="font-size:18px; margin-bottom:1px">Swift Code: ADBLNPKA</p>
  <p style="font-size:18px; margin-bottom:1px">Branch: Satdobato, Lalitpur, Nepal </p>
<p>*While sending money through SWIFT, A/c No, Name of beneficiary and branch should be mentioned.</p>
 </div>  <br><br>
<div class="row text-center g-mb-40">
    <div class="col-md-6">
        <p style="margin-bottom:1px">Option 1</p>

      <h3>Pay with Esewa</h3>
      <h3>Coming Soon</h3>
      {{-- <form id="esewa-init" action="/esewa-init" class="g-pa-10" enctype="multipart/form-data">
        @csrf
        
          <button type="submit" class="btn btn-success" >Pay with Esewa</button>
      </form> --}}
      {{-- <a href="#!" id="esewa-button" class="btn btn-success"></a> --}}
      <form action="https://uat.esewa.com.np/epay/main" method="POST" id="esewa-form">
        <input value="2000" id="tAmt" name="tAmt" type="hidden">
        <input value="2000" id="amt" name="amt" type="hidden">
        <input value="0" name="txAmt" type="hidden">
        <input value="0" name="psc" type="hidden">
        <input value="0" name="pdc" type="hidden">
        <input value="epay_payment" name="scd" type="hidden">
        <input value="" name="pid" id="product_id" type="hidden">
        <input value="{{route('esewa.success')}}" type="hidden" name="su">
        <input value="{{route('esewa.failure')}}" type="hidden" name="fu">
      </form>
    </div>
    <div class="col-md-6">
        <p style="margin-bottom:1px">Option 2</p>
        
        <h3>Upload bank voucher</h3>
        @if(App\User::findOrFail(Auth::user()->id)->paid_status=="rejected")
          <p style="font-size:18px; color:red " class="text-center">Your payment is rejected. Please resubmit valid document or directly contact us in case of any problem.</p>
        @endif  
      <form method="POST" action="/upload-voucher" class="g-pa-10" enctype="multipart/form-data">
      @csrf
        <input class="form-control" required type="file" name="voucher"><br>
        <select class="form-control" name="user_type" required>
          <option value="Nepalese">I am Nepalese</option>
          <option value="Foreigner">I am a foreigner</option>

        </select>
        <br>
        <button type="submit" class="btn btn-primary" >Submit</button>
      </form>
    </div>

@elseif(App\User::findOrFail(Auth::user()->id)->paid_status=="paid")
<div class="text-center g-mb-40">
  <p style="font-size:18px; " class="text-center">Your payment is  <span style="color:green">sucessful</span>. You can now attend the cenference. Please bring the printed form of invoice at the event.</p>
    <a href="/download-invoice" class="btn btn-primary">View Invoice</a>
</div>
@elseif(App\User::findOrFail(Auth::user()->id)->paid_status=="pending")
<div class="text-center g-mb-40">
  <p style="font-size:18px; " class="text-center">Your payment is on verification. We will notify you as soon as your payment is verified.</p>
    
</div>
@endif

  </div>
</div>

      @endsection
@section('js')
      <script>
        $(document).ready(function(){
          

          // this is the id of the form
          $("#esewa-init").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.


            $.ajax({
                  type: "GET",
                  url: "/esewa-init",
                  // serializes the form's elements.
                  success: function(data)
                  {
                    console.log(data);
                      $('#tAmt').val(data['tAmt']);
                      $('#amt').val(data['amt']); 
                      $('#product_id').val(data['p_id']);
                      $('#esewa-form').submit();
                  }
                });


            });
          });
  </script>
@stop