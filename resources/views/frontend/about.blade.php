@extends('layouts.app')
@section('title','About Us')

@section('body')
<div class="row align-items-stretch">
    <div class="col-lg-12 g-mb-30">
        <!-- Article -->
        <article class="text-center g-color-white g-overflow-hidden">
            <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/main2.jpg" style="background-image: url(/img/main2.jpg);">
                <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-py-50 g-px-20">
                
                </div>
            </div>
        </article>
        <!-- End Article -->
    </div>
      
       
</div>
<section class=" g-bg-white-light-v5 g-mb-40">
    <div class="container">
            <div class="text-center g-mb-70">
              <br>
                    <h2 class="text-uppercase g-line-height-1 g-font-weight-700 g-color-black g-font-size-30 g-mb-30">About
                      <span class="g-font-weight-400 g-font-size-40">|</span>
                      <span class="g-color-primary">Us</span></h2>
        
                    
            </div>
            <div class="col-lg-12">
                    <div class="g-mb-60">
                      <p><span class="d-inline-block float-left g-width-60 g-height-60 g-color-black g-font-weight-600 g-font-size-30 text-center g-pa-7 mr-2">N</span>
                        epal Academy of Science and Technology (NAST) is an autonomous apex body established in 1982 to promote science and technology in the country. The Academy is entrusted with four major objectives: advancement of science and technology for all-round development of the nation; preservation and further modernization of indigenous technologies; promotion of research in science and technology; and identification and facilitation of appropriate technology transfer.
                      </p>
                      
                      <div class="fulltext-block-left">
                          <p style="text-align:justify"><span style="font-size:20px;color:black"><strong>Organization Structure</strong></span></p>

                          <p style="text-align:justify">Prime Minister is the Chancellor of the Academy. Chancellor chairs the Academic Assembly, the highest body of NAST. The Academic Assembly normally meets twice a year setting policy guidelines for the Academy and approving the annual program and budget. The Minister for Science and Technology is an ex-officio Pro-chancellor. Other members of the Assembly include the Academicians of NAST, Vice Chairman of National Planning Commission, Vice Chancellors among the universities, three representatives of the S&amp;T professional societies and two staff members of the Academy. The Vice Chancellor, appointed by the Chancellor in recommendation of a special committee, is the Chief Executive, who heads amanagement council, composed of up to five Members of the Academic Assembly and a Member Secretary. NAST's programs are conceived and executed by the Faculties and the Divisions led by senior staff of the Academy.</p>
                          
                          <p style="text-align:justify"><img alt="" src="http://nast.gov.np/demo/ck/filemanager/userfiles/nast_structure.jpg"></p>
                          
                          <p style="text-align:justify"><span style="font-size:18px;color:black"><strong>The Academic Assembly</strong></span></p>
                          
                          <p style="text-align:justify">The highest policy making body of the academy is the Academic Assembly. The Prime Minister is the Chancellor of the Academy and chairs the Assembly. The Academic Assembly normally meets twice a year for setting policy guidelines for the Academy and approving the annual program and budget. The Minister for Science and Technology is an ex-officio Pro-chancellor of the academy. Other members of the Assembly include Vice Chairman of National Planning Commission, Vice Chancellor of NAST, twenty seven members from among NAST academicians, two Vice Chancellors from among the universities, three representatives of S&amp;T professional societies, two associate academicians of NAST, Secretary of Ministry of Science and Technology, two representatives of the Government of Nepal gazetted first class officers, two representatives of industrial sector and two NAST staff representatives. NAST secretary serves as the member secretary of the Assembly.</p>
                          
                          <p style="text-align:justify">&nbsp;</p>
                          
                          <p style="text-align:justify"><span style="font-size:18px;color:black"><strong>The Management Council</strong></span></p>
                          
                          <p style="text-align:justify">The Management Council is responsible mainly for the implementation of the decisions of the academic assembly meetings. The council is also responsible for the preparation of agendas and proposals to be submitted to the academic assembly meetings. The council makes decision necessary to run the academy on the regular basis. It is chaired by the Vice Chancellor. The other members of the council include upto five members of the academic assembly and NAST Secretary as the member secretary.</p>
                          
                          <p style="text-align:justify">&nbsp;</p>
                          
                          <p style="text-align:justify"><span style="font-size:18pxcolor:black"><strong>The service Commission</strong></span></p>
                          
                          <p style="text-align:justify">The service commission of NAST consists of a Chairman, a member Academician and a representative from the public service commission. The chairman of the commission is nominated by the Chancellor. The secretary of the academy serves as the member secretary of the commission. The commission functions as an independent body in matters related to the permanent appointment and promotion of the academy's staff.&nbsp;The Central office of NAST is located in Khumaltar, Lalitpur. The Vice Chancellor is the chief executive authority and the secretary is the administrative head of the academy.</p>
                          
                          <p style="text-align:justify">NAST programs are conceived and executed by the faculties and the divisions led by senior staff of the academy. Also, there are Faculty Coordination &amp; Planning &amp; Development Committee, Science Faculty &amp; Development Committee, Technology Faculty Research &amp; Development Committee and Scientific Sub-Committees to assist in formulation of policy and scientific activities in different areas of Science and Technology.</p>
                          
                          
                          
                          
                          
                          
                          </div>
                     </div>
          
                    {{-- <div class="row">
                      <div class="col-md-6 g-mb-60">
                        <h3 class="h4 g-color-black g-font-weight-600"><span class="g-font-size-25">01.</span> Mr. Robot</h3>
                        <p>Well, duh. USA's Mr. Robot is probably the most accurate and detailed dramatic portrayal ever made of current hacking practices and hacker culture. Its depiction of the cybersecurity community and its broader meditation on the relationship
                          between humans and technology isn't perfect. (Not to mention that Season 2, which aired this year, had some narrative issues.) But overall the show is compelling and full of delightful references and winks to the cybersecurity subculture.</p>
                      </div>
                      <div class="col-md-6 g-mb-60">
                        <img class="img-fluid" src="about1.jpg" alt="Image Description">
                      </div>
                    </div>
          
                    <div class="row">
                      <div class="col-md-6 flex-md-unordered g-mb-60">
                        <h3 class="h4 g-color-black g-font-weight-600"><span class="g-font-size-25">02.</span> Snowden</h3>
                        <p>The story of whistleblower Edward Snowden is the most dramatic spy story of the decade. An Oliver Stone biopic was inevitable. So here it is. As movies almost always do, Snowden eliminates the nuance from this complicated and controversial
                          story, painting Snowden as a hero and the National Security Agency as a one-dimensional villain. But as WIRED pointed out when the movie debuted, Snowden is important because it's the accessible version of events that many Americans will
                          remember.</p>
                      </div>
                      <div class="col-md-6 flex-md-first g-mb-60">
                        <img class="img-fluid" src="about2.jpg" alt="Image Description">
                      </div>
                    </div>
          
                    <div class="row">
                      <div class="col-md-6 g-mb-60">
                        <h2 class="h4 g-color-black g-font-weight-600"><span class="g-font-size-25">03.</span> Person of Interest</h2>
                        <p>It's not every day that a network drama puts the ethics and repercussions of bulk surveillance at the core of its premise, but CBS's Person of Interest managed to do it successfully. The show combines government surveillance to stop terrorist
                          attacks with an eccentric hacker billionaire and vigilante justice. It even grapples with questions of how an artificial intelligence evolves and incorporates biases. Person of Interest is particularly known for airing a prescient episode
                          in 2012 about a National Security Agency whistleblower very similar to Edward Snowden.</p>
                      </div>
                      <div class="col-md-6 g-mb-60">
                        <img class="img-fluid" src="about3.jpg" alt="Image Description">
                      </div>
                    </div>
          
                    <h2 class="g-color-black g-font-weight-600 text-center g-mb-30">Selecting the Right Movie</h2>
                    <p>It's important to stay detail oriented with every project we tackle. Staying focused allows us to turn every project we complete into something we love. We strive to embrace and drive change in our industry which allows us to keep our clients
                      relevant and ready to adapt. As creatives, it's important that we strive to do work outside of obligation. This lets us stay ahead of the curve for our clients and internal projects. At the end of the day, it's important to not let being busy
                      distract us from having fun. Smiling, laughing, and hanging helps us work together to achieve this.</p> --}}
                  </div>
        
    </div>
</section>
@endsection