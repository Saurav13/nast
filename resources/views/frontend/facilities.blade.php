@extends('layouts.app')
@section('title','Facilities')

@section('body')
<section class=" g-bg-white-light-v5 g-mb-40 g-mt-100">
    <div class="container">
        <div class="g-max-width-550 text-center mx-auto g-mb-100">
            <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-18 g-letter-spacing-2 mb-4">Our Facilities</h1>
            <p class="g-font-size-16">Unify creative technology company providing key digital services. Focused on helping our clients to build a successful business on web and mobile.</p>
        </div>

        <div class="row">
        @foreach($facilities as $facility)
            <div class="col-lg-4 g-mb-30">
              <!-- Article -->
              <article class="u-block-hover g-brd-around g-brd-gray-light-v4">
                <figure class="g-overflow-hidden">
                  <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/facility_images/{{$facility->image}}" alt="Image Description">
                </figure>
          
                <div class="g-pa-30-30-20">
                  <h3 class="h6 text-uppercase g-font-weight-600 g-mb-10">
                    <a class="g-color-gray-dark-v2 g-text-underline--none--hover" href="#">{{$facility->title}}</a>
                  </h3>
          
                  <p>{!!$facility->description!!}</p>
                </div>
              </article>
              <!-- End Article -->
            </div>
          @endforeach
        </div>
        {{$facilities->links()}}
    </div>
</section>