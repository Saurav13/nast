<style>
  .application{
    border:2px solid #21a79b;
    border-radius:20px;
    padding:2rem;
  }
  </style>
<div>

        <div class="text-center">
                
          <h2 class="h3 g-color-black text-uppercase mb-2">Abstract Submitted!!</h2>
          <br>
          <p class="simple_p g-color-black"> Thank you for your abstract submission. <br>  We will soon be in touch with you. Stay Updated!!!.</p>

        </div>

        <div class="container application">
          <h2 class="h3 g-color-black mb-2">Your Application Details:</h2><br>

          <label class="label">Title:</label>
          <p class="simple_p">{{$application->title}}</p>
          <label class="label">Topic:</label>
          <p class="simple_p">{{$application->topic}}</p>
          <label class="label">Theme:</label>
          <ul>
              @foreach (json_decode($application->theme) as $theme)
                <li><p class="simple_p" style="margin-bottom:0">{{ $theme }}</p></li>
              @endforeach
          </ul>

          <label class="label">Abstract:</label>
          <a  download href="/getfile/{{$application->id}}/{{$application->abstract_file}}">{{$application->abstract_file}}</a>

            
        </div>
</div>
