<style>
    .u-check{
        color:black;
    }
    
</style>


<div class="card g-py-20 g-px-40" style="background:white">
        <div class="text-center">
                
                <h2 class="h3 g-color-black text-uppercase mb-2">Abstract Submission</h2>
                <p style="font-size:18px;">Please review our guidelines before submitting your abstract.<br> <a href="/guidelines.docx" style="color:green" download>Click here</a> to download guidelines</p>
        </div>
    <form class="g-py-15"  method="POST" action="{{ route('abstract-submission') }}"  enctype="multipart/form-data" >
        {{csrf_field()}}
    
        <br>
        @if(count($errors))
            <span class="invalid-feedback" role="alert" style="text-align:center">
                <strong>Please provide all the information.</strong>
            </span>
        @endif
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 mb-4">
                <label class="g-color-black g-mb-20">Title of Paper</label>
                    <textarea  style="height:6rem;" class="form-control g-color-black g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" placeholder="Title of your abstract" required>{{ old('title') }}</textarea>
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-xs-12 col-sm-12 mb-4">
                <label class="g-color-black g-mb-20">Topic</label>
                    <select id="topic" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-5 g-px-15 {{ $errors->has('topic') ? ' is-invalid' : '' }}" name="topic">
                        <option value="Health and Medicine" {{ old('topic') == 'Health and Medicine' ? 'selected' : '' }}>Health and Medicine</option>
                        <option value="Water and Energy" {{ old('topic') == 'Water and Energy' ? 'selected' : '' }}>Water and Energy</option>
                        <option value="Food and Agriculture" {{ old('topic') == 'Food and Agriculture' ? 'selected' : '' }}>Food and Agriculture</option>
                        <option value="Transport and Tourism" {{ old('topic') == 'Transport and Tourism' ? 'selected' : '' }}>Transport and Tourism</option>
                        <option value="Information and Communication Technology" {{ old('topic') == 'Information and Communication Technology' ? 'selected' : '' }}>Information and Communication Technology</option>
                        <option value="Forest and Environment" {{ old('topic') == 'Forest and Environment' ? 'selected' : '' }}>Forest and Environment</option>
                        <option value="Disaster Risk Reduction" {{ old('topic') == 'Disaster Risk Reduction' ? 'selected' : '' }}>Disaster Risk Reduction</option>
                        <option value="Indigenous Knowledge and Technology" {{ old('topic') == 'Indigenous Knowledge and Technology' ? 'selected' : '' }}>Indigenous Knowledge and Technology</option>
                        <option value="Fundamental Sciences" {{ old('topic') == 'Fundamental Sciences' ? 'selected' : '' }}>Fundamental Sciences</option>
                        <option value="Applied Sciences" {{ old('topic') == 'Applied Sciences' ? 'selected' : '' }}>Applied Sciences</option>
                        <option value="Emerging Technology" {{ old('topic') == 'Emerging Technology' ? 'selected' : '' }}>Emerging Technology</option>
                        <option value="Clean and Green Sciences" {{ old('topic') == 'Clean and Green Sciences' ? 'selected' : '' }}>Clean and Green Sciences</option>
                        <option value="Economics and Management" {{ old('topic') == 'Economics and Management' ? 'selected' : '' }}>Economics and Management</option>
                        <option value="Policy" {{ old('topic') == 'Policy' ? 'selected' : '' }}>Policy</option>
                        <option value="Philosophy" {{ old('topic') == 'Philosophy' ? 'selected' : '' }}>Philosophy</option>
                        
                        <option value="Others" {{ old('topic') == 'Others' ? 'selected' : '' }}>Others</option>
                    </select>
                    <br>
                    <input type="text" id="other_value" name="other_value" {{ old('topic') == 'Others' ? '' : 'hidden' }} class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" value="{{ old('other_value') }}" placeholder="If Others, Specify" >
                    @if ($errors->has('topic') || $errors->has('other_value'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('topic') }} {{ $errors->first('other_value') }}</strong>
                        </span>
                    @endif
            </div>
            <div class="col-xs-12 col-sm-12 mb-4">
                <label class="g-color-black g-mb-20">Theme (You can choose multiple themes)</label>
                <div class="row g-mb-30">
                    @php
                        $themes = old('theme') ? : [];
                    @endphp
                        <!-- Left Column -->
                    <div class="col-md-6">
                        <div class="form-group g-mb-10">
                        <label class="u-check g-pl-25">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Science for society" type="checkbox" {{ in_array('Science for society',$themes) ? 'checked' : '' }}>
                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon=""></i>
                            </div>
                            Science for society 
                        </label>
                        </div>
                
                        <div class="form-group g-mb-10">
                        <label class="u-check g-pl-25">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Innovation for Prosperity" type="checkbox" {{ in_array('Innovation for Prosperity',$themes) ? 'checked' : '' }}>
                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon=""></i>
                            </div>
                            Innovation for Prosperity
                        </label>
                        </div>
                
                        <div class="form-group g-mb-10 g-mb-0--md">
                        <label class="u-check g-pl-25">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Institutional Reforms" type="checkbox" {{ in_array('Institutional Reforms',$themes) ? 'checked' : '' }}>
                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon=""></i>
                            </div>
                            Institutional Reforms 
                        </label>
                        </div>
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Investment in Science, Infrastructure development, identification, mobilization, and management of resources" type="checkbox" {{ in_array('Investment in Science, Infrastructure development, identification, mobilization, and management of resources',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Investment in Science, Infrastructure development, identification, mobilization, and management of resources

                            </label>
                        </div>
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Science for Security" type="checkbox" {{ in_array('Science for Security',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Science for Security  
                            </label>
                        </div>
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Science, Technology, and Innovation for  SDGs" type="checkbox" {{ in_array('Science, Technology, and Innovation for SDGs',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Science, Technology, and Innovation for  SDGs
                            </label>
                        </div>
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Youth empowerment for science, technology, and innovation" type="checkbox" {{ in_array('Youth empowerment for science, technology, and innovation',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Youth empowerment for science, technology, and innovation 
                            </label>
                        </div>
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Science Diplomacy" type="checkbox" {{ in_array('Science Diplomacy',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Science Diplomacy 
                            </label>
                        </div>
                    </div>
                    <!-- End Left Column -->
                
                    <!-- Right Column -->
                    <div class="col-md-6">
                        <div class="form-group g-mb-10">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="STI Policy" type="checkbox" {{ in_array('STI Policy',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                STI Policy
                            </label>
                        </div>
                
                        <div class="form-group g-mb-10">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Academia-Government-Industries partnership for STI" type="checkbox" {{ in_array('Academia-Government-Industries partnership for STI',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Academia-Government-Industries partnership for STI
                            </label>
                        </div>
                
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Scientific Data Management" type="checkbox" {{ in_array('Scientific Data Management',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Scientific Data Management
                            </label>
                        </div>
                        <div class="form-group g-mb-10">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Indigenous and Traditional Knowlegde" type="checkbox" {{ in_array('Indigenous and Traditional Knowlegde',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Indigenous and Traditional Knowlegde
                            </label>
                        </div>
                
                        <div class="form-group g-mb-10">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Intellectual Property Right" type="checkbox" {{ in_array('Intellectual Property Right',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Intellectual Property Right 
                            </label>
                        </div>
                
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Standardization and Quality Assuranced" type="checkbox" {{ in_array('Standardization and Quality Assuranced',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Standardization and Quality Assuranced
                            </label>
                        </div>
                        <div class="form-group g-mb-10">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Identification and Prioritization of key areas in STI" type="checkbox" {{ in_array('Identification and Prioritization of key areas in STI',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Identification and Prioritization of key areas in STI
                            </label>
                        </div>
                
                        <div class="form-group g-mb-10 g-mb-0--md">
                            <label class="u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="theme[]" value="Scientific tourism" type="checkbox" {{ in_array('Scientific tourism',$themes) ? 'checked' : '' }}>
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                                </div>
                                Scientific tourism
                            </label>
                        </div>  
                    </div>
                        <!-- End Right Column -->
                </div>   
                @if ($errors->has('theme'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('theme') }}</strong>
                    </span>
                @endif
            </div>
        
            <div class="col-xs-12 col-sm-12 mb-4">
                <label class="g-color-black g-mb-20"> Upload Extended Abstract as per the template in guidelines. </label><br>
                <input type="file" name="abstract_file" required />
        
                <br><br>
                <span>Only doc and docx files are accepted.</p>
                @if ($errors->has('abstract_file'))
                    <span class="invalid-feedback" role="alert">
                    
                        <strong>{{ $errors->first('abstract_file') }}</strong>
                    </span>
                @endif
                </div>
        </div>
        
        

        <div class="row justify-content-between mb-5">
            
            <div class="col align-self-center ">
                <br>
                <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Submit</button>
            </div>
        </div>
    </form>
</div>
