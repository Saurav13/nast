<style>
        .application{
          border:2px solid #21a79b;
          border-radius:20px;
          padding:2rem;
        }
        </style>
      <div>
      
              <div class="text-center">
                      
                      <h2 class="h3 g-color-black text-uppercase mb-2">Application Status: </h2>
                      <br>
          <p class="simple_p"> We regret to inform you that papar has been rejected for this conference. <br> We highly appreciate the effort you put for this paper. </p>
      
              </div>
      
                  <div class="container application">
                      <h2 class="h3 g-color-black mb-2">Your Application Details:</h2><br>
      
                      <label class="label">Title:</label>
                      <p class="simple_p">{{$application->title}}</p>
                      <label class="label">Topic:</label>
                      <p class="simple_p">{{$application->topic}}</p>
                      <label class="label">Theme:</label>
                      <ul>
                        @foreach (json_decode($application->theme) as $theme)
                          <li><p class="simple_p" style="margin-bottom:0">{{ $theme }}</p></li>
                        @endforeach
                      </ul>
                      <label class="label">File:</label>
                  <a  download href="/abstracts/{{$application->abstract_file}}">{{$application->abstract_file}}</a>
                 <br>
                 {{-- <form action="/re-apply" method="POST">
                    {{csrf_field()}}
                  <button type="submit" class="btn btn-primary">Re-Submit</button>
                 </form> --}}
                  </div>
      </div>
      